package ru.omsu.imit.course3;

import org.junit.Test;

public class TestCityList {
    @Test
    public void testList() {
        CityList list = new CityList();
        City omsk = new City("Omsk", 3456, "Город");
        City novotroickoe = new City("Новотроецкое", 567, "Посёлок городского типа");
        City beregovoy = new City("Береговой", 230, "Деревня");

        list.add(omsk);
        list.add(novotroickoe);
        list.add(beregovoy);

        list.removeOf(0);
        list.removeOf(1);

        list.remove(beregovoy);

    }
}
