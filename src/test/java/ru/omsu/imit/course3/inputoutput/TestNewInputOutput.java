package ru.omsu.imit.course3.inputoutput;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import ru.omsu.imit.course3.file.FileService;
import ru.omsu.imit.course3.trainee.Trainee;
import ru.omsu.imit.course3.trainee.TraineeException;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class TestNewInputOutput {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testReadTraineeFromFileByteBuffer() throws NumberFormatException, TraineeException {
        Trainee traineeToWrite = new Trainee("Лука", "Лукич", 2);
        try {
            File file = folder.newFile("test.txt");
            FileService.writeTraineeToTextFileOneLine(file, traineeToWrite);
            assertTrue(file.exists());
            assertEquals(1, Files.readAllLines(file.toPath()).size());
            Trainee traineeRead = NewInputOutput.readTraineeFromFileByteBuffer(file);
            assertEquals(traineeToWrite, traineeRead);
        } catch (IOException ex) {
            fail();
        }
    }

    @Test
    public void testReadTraineeFromFileMappedByteBuffer() throws NumberFormatException, TraineeException {
        Trainee traineeToWrite = new Trainee("Афонасий", "Афонасьевич", 2);
        try {
            File file = folder.newFile("test.txt");
            FileService.writeTraineeToTextFileOneLine(file, traineeToWrite);
            assertTrue(file.exists());
            assertEquals(1, Files.readAllLines(file.toPath()).size());
            Trainee traineeRead = NewInputOutput.readTraineeFromFileMappedByteBuffer(file);
            assertEquals(traineeToWrite, traineeRead);
        } catch (IOException ex) {
            fail();
        }
    }

    @Test
    public void testWriteReadNumArrayToFileMappedByteBuffer() {
        try {
            File file = folder.newFile("test.txt");
            int[] intToWrite = IntStream.range(0, 99).toArray();
            NewInputOutput.writeNumArrayToFileMappedByteBuffer(file, intToWrite);
            assertTrue(file.exists());
            byte[] byteRead = FileService.readByteArrayFromBinaryFile(file);
            ByteBuffer byteBuffer = ByteBuffer.allocate(intToWrite.length * 4);
            byteBuffer.asIntBuffer().put(intToWrite);
            assertArrayEquals(byteRead, byteBuffer.array());
        } catch (IOException ex) {
            fail();
        }
    }

    @Test
    public void testSerializeTraineeToByteByffer() throws TraineeException {
        try {
            File file = folder.newFile("test.dat");
            Trainee trainee = new Trainee("Вася", "Васильевич", 5);
            FileService.serializeTraineeToBinaryFile(file, trainee);
            assertTrue(file.exists());
            byte[] byteRead = FileService.readByteArrayFromBinaryFile(file);
            ByteBuffer serializeTrainee = NewInputOutput.serializeTraineeToByteByffer(trainee);
            assertEquals(ByteBuffer.wrap(byteRead), serializeTrainee);
        } catch (IOException ex) {
            fail();
        }
    }
}
