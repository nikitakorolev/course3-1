package ru.omsu.imit.course3.color;

import org.junit.Test;

import java.util.EnumSet;
import java.util.stream.Collector;

import static org.junit.Assert.*;

public class TestColor {
    @Test
    public void testColorEnumSet(){
        EnumSet<Color> allElem = EnumSet.allOf(Color.class);
        EnumSet<Color> enumSetAllElem = EnumSet.of(Color.ORANGE, Color.VIOLET, Color.YELLOW);
        EnumSet<Color> enumSetOneElem = EnumSet.of(Color.ORANGE);
        EnumSet<Color> enumSetNoElem = EnumSet.noneOf(Color.class);
        EnumSet<Color> enumSetRangeElem = EnumSet.range(Color.VIOLET, Color.YELLOW);

        assertTrue(enumSetAllElem.contains(Color.ORANGE));
        assertFalse(enumSetNoElem.contains(Color.VIOLET));
        assertTrue(enumSetOneElem.contains(Color.ORANGE));
        assertTrue(enumSetRangeElem.contains(Color.YELLOW));
    }

    @Test
    public void testColors() {
        Color[] colors = Color.values();
        assertEquals(3, colors.length);
        assertEquals(0, Color.VIOLET.ordinal());
        assertEquals(1, Color.ORANGE.ordinal());
        assertEquals(2, Color.YELLOW.ordinal());
    }

    @Test
    public void colorFromString() {
        try {
            Color.colorFromString("RED");
        } catch (ColorException ex) {
            assertEquals(ColorErrorCode.WRONG_COLOR_STRING.getErrorString(), ex.getErrorCode().getErrorString());
        }

        try {
            Color.colorFromString(null);
        } catch (ColorException ex) {
            assertEquals(ColorErrorCode.NULL_COLOR.getErrorString(), ex.getErrorCode().getErrorString());
        }
    }
}
