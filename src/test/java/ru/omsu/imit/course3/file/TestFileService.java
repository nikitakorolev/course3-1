package ru.omsu.imit.course3.file;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import ru.omsu.imit.course3.figures.Rectangle;
import ru.omsu.imit.course3.trainee.Trainee;
import ru.omsu.imit.course3.trainee.TraineeException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Random;

import static org.junit.Assert.*;

public class TestFileService {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testFileReadWriteRectangleBinary() {
        Rectangle rectToWrite = new Rectangle(20, 20, 30, 30);
        try {
            File file = folder.newFile("test.dat");
            FileService.writeRectangleToBinaryFile(file, rectToWrite);
            assertTrue(file.exists());
            assertEquals(16, file.length());
            Rectangle rectRead = FileService.readRectangleFromBinaryFile(file);
            assertEquals(rectToWrite, rectRead);
        } catch (IOException ex) {
            fail();
        }
    }

    @Test
    public void testFileReadWriteRectangleArrayBinary() {
        int count = 5;
        Rectangle[] rectsToWrite = new Rectangle[count];
        Random random = new Random();
        for (int i = 0; i < count; i++) {
            rectsToWrite[i] = new Rectangle(random.nextInt(), random.nextInt(), random.nextInt(), random.nextInt());
        }
        try {
            File file = folder.newFile("test.dat");
            FileService.writeRectangleArrayToBinaryFile(file, rectsToWrite);
            assertTrue(file.exists());
            assertEquals(count * 16, file.length());
            Rectangle[] rectsRead = FileService.readRectangleArrayFromBinaryFileReverse(file);
            for (int i = 0; i < rectsRead.length / 2; i++) {
                Rectangle temp = rectsRead[i];
                rectsRead[i] = rectsRead[rectsRead.length - i - 1];
                rectsRead[rectsRead.length - i - 1] = temp;
            }

            assertArrayEquals(rectsToWrite, rectsRead);
        } catch (IOException ex) {
            fail();
        }
    }

    @Test
    public void testFileReadWriteTraineeFromTextThreeLine() throws NumberFormatException, TraineeException {
        Trainee traineeToWrite = new Trainee("Иван", "Васечкин", 5);
        try {
            File file = folder.newFile("test.dat");
            FileService.writeTraineeToTextFileThreeLines(file, traineeToWrite);
            assertTrue(file.exists());
            assertEquals(3, Files.readAllLines(file.toPath()).size());
            Trainee traineeRead = FileService.readTraineeFromTextFileThreeLine(file);
            assertEquals(traineeToWrite, traineeRead);
        } catch (IOException ex) {
            fail();
        }
    }

    @Test
    public void testFileReadWriteTraineeFromTextOneLine() throws NumberFormatException, TraineeException {
        Trainee traineeToWrite = new Trainee("Лука", "Лукич", 2);
        try {
            File file = folder.newFile("test.txt");
            FileService.writeTraineeToTextFileOneLine(file, traineeToWrite);
            assertTrue(file.exists());
            assertEquals(1, Files.readAllLines(file.toPath()).size());
            Trainee traineeRead = FileService.readTraineeFromTextFileOneLine(file);
            assertEquals(traineeToWrite, traineeRead);
        } catch (IOException ex) {
            fail();
        }
    }

    @Test
    public void testFileSerializeDeserializeTraineeBinary() throws TraineeException, ClassNotFoundException {
        Trainee traineeToWrite = new Trainee("Лаврентий", "Лавреньтич", 4);
        try {
            File file = folder.newFile("test.txt");
            FileService.serializeTraineeToBinaryFile(file, traineeToWrite);
            assertTrue(file.exists());
            Trainee traineeRead = FileService.deserializeTraineeFromBinaryFile(file);
            assertEquals(traineeToWrite, traineeRead);
        } catch (IOException ex) {
            fail();
        }
    }

    @Test
    public void testStringSerializeDeserializeTraineeJson() throws TraineeException {
        Trainee traineeToWrite = new Trainee("Гоша", "Гошевич", 1);
        String json = FileService.serializeTraineeToJsonString(traineeToWrite);
        Trainee traineeRead = FileService.deserializeTraineeFromJsonString(json);
        assertEquals(traineeToWrite, traineeRead);
    }

    @Test
    public void testFileSerializeDeserializeTraineeJson() throws TraineeException {
        Trainee traineeToWrite = new Trainee("Глеб", "Глебрвич", 5);
        try {
            File file = folder.newFile("test.txt");
            FileService.serializeTraineeToJsonFile(file, traineeToWrite);
            assertTrue(file.exists());
            Trainee traineeRead = FileService.deserializeTraineeFromJsonFile(file);
            assertEquals(traineeToWrite, traineeRead);
        } catch (IOException ex) {
            fail();
        }
    }

    @Test
    public void testRenameFileExtension() {
        try {
            File dir = folder.newFolder("test");
            File file1 = new File(dir.getPath() + "/test1.dat");
            File file2 = new File(dir.getPath() + "/test2.txt");
            File file3 = new File(dir.getPath() + "/test3.dat");
            file1.createNewFile();
            file2.createNewFile();
            file3.createNewFile();
            assertEquals(3, dir.listFiles().length);
            FileService.renameFileExtension(dir);
            assertEquals(dir.getPath() + "\\test1.bin", dir.listFiles()[0].toString());
            assertEquals(dir.getPath() + "\\test3.bin", dir.listFiles()[2].toString());
        } catch (IOException ex) {
            fail();
        }
    }
}
