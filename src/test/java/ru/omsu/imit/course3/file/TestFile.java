package ru.omsu.imit.course3.file;

import com.google.common.io.Files;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.annotation.Target;

import static org.junit.Assert.*;

public class TestFile {
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void testCreateDeleteRenameFile() {
        try {
            File file = folder.newFile("test.txt");
            file.createNewFile();
            File newFileName = folder.newFile("newTestName.txt");
            file.renameTo(newFileName);
            assertTrue(newFileName.exists());
            assertTrue(newFileName.delete());
        } catch (IOException ex) {
            fail();
        }
    }

    @Test
    public void testRenameDirectory() {
        try {
            File dir1 = folder.newFolder("test");
            assertTrue(dir1.exists());
            File newDir1 = folder.newFolder("newTestName");
            dir1.renameTo(newDir1);
            assertTrue(newDir1.exists());
            assertTrue(newDir1.delete());
        } catch (IOException ex) {
            fail();
        }
    }

    @Test
    public void testFileType() {
        File file = new File("E:\\Программирование серверных приложений\\Новый текстовый документ.txt");
        assertTrue(file.exists());
        assertEquals("txt", Files.getFileExtension("E:\\Программирование серверных приложений\\Новый текстовый документ.txt"));
    }

    @Test
    public void testFileList() {
        try {
            File dir = folder.newFolder("test");
            folder.newFile("test/test1.txt");
            folder.newFile("test/test2.txt");
            folder.newFile("test/test3.dat");

            String[] allFilesInDir = dir.list();
            assertArrayEquals(new String[]{"test1.txt", "test2.txt", "test3.dat"}, allFilesInDir);

            String[] maskFilesInDir = dir.list(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".dat");
                }
            });
            assertArrayEquals(new String[]{"test3.dat"}, maskFilesInDir);
        } catch (IOException ex) {
            fail();
        }
    }
}
