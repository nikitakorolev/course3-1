package ru.omsu.imit.course3.trainee;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestGroup {
    @Test
    public void testEmptyGroup() throws TraineeException {
        Trainee[] trainees = {new Trainee("Феофан", "Феофанович", 4)};
        Group group = new Group("МПБ", trainees);
        assertEquals("МПБ", group.getName());
        assertEquals(1, group.getTrainees().length);
        group.setName("ММС");
        assertEquals("ММС", group.getName());
    }


    @SuppressWarnings("unused")
    @Test
    public void testWrongName() throws TraineeException {
        Trainee[] trainees = {new Trainee("Феофан", "Феофанович", 4)};
        try {
            Group group = new Group("", trainees);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.GROUP_WRONG_NAME, ex.getErrorCodes());
        }

        try {
            Group group = new Group(null, trainees);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.GROUP_WRONG_NAME, ex.getErrorCodes());
        }

        try {
            Group group = new Group("МПБ", trainees);
            group.setName("");
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.GROUP_WRONG_NAME, ex.getErrorCodes());
        }

        try {
            Group group = new Group("МПБ", trainees);
            group.setName(null);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.GROUP_WRONG_NAME, ex.getErrorCodes());
        }
    }

    @SuppressWarnings("unused")
    @Test
    public void testWrongTrainees() {
        try {
            Group group = new Group("МПБ", null);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.GROUP_WRONG_TRAINEES, ex.getErrorCodes());
        }

        try {
            Group group = new Group("МПБ", new Trainee[3]);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.GROUP_WRONG_TRAINEES, ex.getErrorCodes());
        }

        try {
            Trainee[] trainees = {new Trainee("Женя", "Женевич", 3)};
            Group group = new Group("МПБ", trainees);
            group.setTrainees(null);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.GROUP_WRONG_TRAINEES, ex.getErrorCodes());
        }

        try {
            Trainee[] trainees = {new Trainee("Феофан", "Феофанович", 4)};
            Group group = new Group("МПБ", trainees);
            group.setTrainees(new Trainee[3]);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.GROUP_WRONG_TRAINEES, ex.getErrorCodes());
        }
    }

    @Test
    public void testSortTraineeListByGrade() throws TraineeException {
        Trainee trainee1 = new Trainee("Иван", "Иванов", 1);
        Trainee trainee2 = new Trainee("Федор", "Фёдоров", 2);
        Trainee trainee3 = new Trainee("Петр", "Петров", 4);
        Trainee trainee4 = new Trainee("Александр", "Александрович", 5);
        Trainee[] trainees = {trainee1, trainee2, trainee3, trainee4};
        Group group = new Group("МПБ", trainees);
        assertEquals(4, group.getTrainees().length);
        group.sortTraineesByGrade();
        assertEquals(trainee4, group.getTrainees()[0]);
        assertEquals(trainee3, group.getTrainees()[1]);
        assertEquals(trainee2, group.getTrainees()[2]);
        assertEquals(trainee1, group.getTrainees()[3]);
    }

    @Test
    public void testSortTraineeListByFirstName() throws TraineeException {
        Trainee trainee1 = new Trainee("Иван", "Иванов", 5);
        Trainee trainee2 = new Trainee("Федор", "Фёдоров", 2);
        Trainee trainee3 = new Trainee("Петр", "Петров", 3);
        Trainee trainee4 = new Trainee("Александр", "Александрович", 1);
        Trainee[] trainees = {trainee1, trainee2, trainee3, trainee4};
        Group group = new Group("МПБ", trainees);
        assertEquals(4, group.getTrainees().length);
        group.sortTraineesByFirstName();
        assertEquals(trainee4, group.getTrainees()[0]);
        assertEquals(trainee1, group.getTrainees()[1]);
        assertEquals(trainee3, group.getTrainees()[2]);
        assertEquals(trainee2, group.getTrainees()[3]);
    }

    @Test
    public void testFindTraineeByFirstName() throws TraineeException {
        Trainee trainee1 = new Trainee("Илья", "Ильич", 4);
        Trainee trainee2 = new Trainee("Костя", "Константинович", 3);
        Trainee[] trainees = {trainee1, trainee2};
        Group group = new Group("ММС", trainees);
        assertEquals(2, group.getTrainees().length);
        Trainee found = group.getTraineeByFirstName("Илья");
        assertEquals(trainee1, found);
        try {
            group.getTraineeByFirstName("Лука");
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.TRAINEE_NOT_FOUND, ex.getErrorCodes());
        }
    }
}
