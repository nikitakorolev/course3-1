package ru.omsu.imit.course3.trainee;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestTraineeList {
    @Test
    public void testFindTraineeByFirstName() throws TraineeException {
        TraineeList traineeList = new TraineeList();
        Trainee trainee1 = new Trainee("Иван", "Иванов", 1);
        Trainee trainee2 = new Trainee("Петр", "Петров", 2);
        traineeList.addTrainee(trainee1);
        traineeList.addTrainee(trainee2);
        assertEquals(2, traineeList.getTrainees().size());
        Trainee found = traineeList.getTraineeByFirstName("Петр");
        assertEquals(trainee2, found);
        try {
            traineeList.getTraineeByFirstName("Женя");
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.TRAINEE_NOT_FOUND, ex.getErrorCodes());
        }
    }

    @Test
    public void testSortTraineeListByFirstName() throws TraineeException {
        TraineeList traineeList = new TraineeList();
        Trainee trainee1 = new Trainee("Иван", "Иванов", 1);
        Trainee trainee2 = new Trainee("Федор", "Петров", 2);
        Trainee trainee3 = new Trainee("Петр", "Иванов", 2);
        Trainee trainee4 = new Trainee("Александр", "Николаев", 2);
        traineeList.addTrainee(trainee1);
        traineeList.addTrainee(trainee2);
        traineeList.addTrainee(trainee3);
        traineeList.addTrainee(trainee4);
        assertEquals(4, traineeList.getTrainees().size());
        traineeList.sortTraineeListByFirstName();
        assertEquals(trainee4, traineeList.getTrainees().get(0));
        assertEquals(trainee1, traineeList.getTrainees().get(1));
        assertEquals(trainee3, traineeList.getTrainees().get(2));
        assertEquals(trainee2, traineeList.getTrainees().get(3));
    }

    @Test
    public void testReverseTraineeList() throws TraineeException {
        TraineeList traineeList = new TraineeList();
        Trainee trainee1 = new Trainee("Иван", "Иванов", 1);
        Trainee trainee2 = new Trainee("Федор", "Петров", 2);
        Trainee trainee3 = new Trainee("Петр", "Иванов", 2);
        Trainee trainee4 = new Trainee("Александр", "Николаев", 2);
        traineeList.addTrainee(trainee1);
        traineeList.addTrainee(trainee2);
        traineeList.addTrainee(trainee3);
        traineeList.addTrainee(trainee4);
        assertEquals(4, traineeList.getTrainees().size());
        traineeList.reverseTraineeList();
        assertEquals(trainee4, traineeList.getTrainees().get(0));
        assertEquals(trainee3, traineeList.getTrainees().get(1));
        assertEquals(trainee2, traineeList.getTrainees().get(2));
        assertEquals(trainee1, traineeList.getTrainees().get(3));
    }

    @Test
    public void testRotateTraineeList() throws TraineeException {
        TraineeList traineeList = new TraineeList();
        Trainee trainee1 = new Trainee("Иван", "Иванов", 1);
        Trainee trainee2 = new Trainee("Федор", "Петров", 2);
        Trainee trainee3 = new Trainee("Петр", "Иванов", 2);
        Trainee trainee4 = new Trainee("Александр", "Николаев", 2);
        traineeList.addTrainee(trainee1);
        traineeList.addTrainee(trainee2);
        traineeList.addTrainee(trainee3);
        traineeList.addTrainee(trainee4);
        assertEquals(4, traineeList.getTrainees().size());
        traineeList.rotateTraineeList(2);
        assertEquals(trainee1, traineeList.getTrainees().get(2));
        assertEquals(trainee2, traineeList.getTrainees().get(3));
        assertEquals(trainee3, traineeList.getTrainees().get(0));
        assertEquals(trainee4, traineeList.getTrainees().get(1));
    }

    @Test
    public void testGetTraineeWithMaxGrade() throws TraineeException {
        TraineeList traineeList = new TraineeList();
        Trainee trainee1 = new Trainee("Иван", "Иванов", 5);
        Trainee trainee2 = new Trainee("Федор", "Петров", 2);
        Trainee trainee3 = new Trainee("Петр", "Иванов", 5);
        Trainee trainee4 = new Trainee("Александр", "Николаев", 1);
        traineeList.addTrainee(trainee1);
        traineeList.addTrainee(trainee2);
        traineeList.addTrainee(trainee3);
        traineeList.addTrainee(trainee4);
        assertEquals(4, traineeList.getTrainees().size());
        List<Trainee> maxGrade = traineeList.getTraineeWithMaxGrade();
        assertEquals(2, maxGrade.size());
        assertEquals(5, maxGrade.get(0).getGrade());
        assertEquals(5, maxGrade.get(1).getGrade());
    }

    @Test
    public void testGetTraineeWithMaxGradeFromEmptyList() throws TraineeException {
        TraineeList traineeList = new TraineeList();
        try {
            @SuppressWarnings("unused")
            List<Trainee> maxGrade = traineeList.getTraineeWithMaxGrade();
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.TRAINEE_NOT_FOUND, ex.getErrorCodes());
        }
    }

    @Test
    public void testSortTraineeListByGrade() throws TraineeException {
        TraineeList traineeList = new TraineeList();
        Trainee trainee1 = new Trainee("Иван", "Иванов", 2);
        Trainee trainee2 = new Trainee("Федор", "Петров", 3);
        Trainee trainee3 = new Trainee("Петр", "Иванов", 4);
        Trainee trainee4 = new Trainee("Александр", "Николаев", 5);
        traineeList.addTrainee(trainee1);
        traineeList.addTrainee(trainee2);
        traineeList.addTrainee(trainee3);
        traineeList.addTrainee(trainee4);
        assertEquals(4, traineeList.getTrainees().size());
        traineeList.sortTraineeListByGrade();
        assertEquals(trainee4, traineeList.getTrainees().get(0));
        assertEquals(trainee3, traineeList.getTrainees().get(1));
        assertEquals(trainee2, traineeList.getTrainees().get(2));
        assertEquals(trainee1, traineeList.getTrainees().get(3));
    }
}
