package ru.omsu.imit.course3.trainee;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestTrainee {
    @Test
    public void testTrainee() throws TraineeException {
        Trainee trainee = new Trainee("Сергей", "Сергеев", 3);
        assertEquals("Сергей", trainee.getFirstName());
        assertEquals("Сергеев", trainee.getLastName());
        assertEquals(3, trainee.getGrade());
        trainee.setFirstName("Петр");
        assertEquals("Петр", trainee.getFirstName());
        trainee.setLastName("Петров");
        assertEquals("Петров", trainee.getLastName());
        trainee.setGrade(2);
        assertEquals(2, trainee.getGrade());
    }

    @SuppressWarnings("unused")
    @Test
    public void testWrongFirstName() throws TraineeException {
        try {
            Trainee trainee = new Trainee(null, "Сидоров", 2);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.TRAINEE_WRONG_FIRSTNAME, ex.getErrorCodes());
        }

        try {
            Trainee trainee = new Trainee("", "Лапочкин", 5);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.TRAINEE_WRONG_FIRSTNAME, ex.getErrorCodes());
        }

        try {
            Trainee trainee = new Trainee("Антон", "Антонов", 2);
            trainee.setFirstName(null);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.TRAINEE_WRONG_FIRSTNAME, ex.getErrorCodes());
        }

        try {
            Trainee trainee = new Trainee("Андрей", "Андреев", 1);
            trainee.setFirstName("");
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.TRAINEE_WRONG_FIRSTNAME, ex.getErrorCodes());
        }
    }

    @SuppressWarnings("unused")
    @Test
    public void testWrongLastName() throws TraineeException {
        try {
            Trainee trainee = new Trainee("Игорь", null, 2);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.TRAINEE_WRONG_LASTNAME, ex.getErrorCodes());
        }

        try {
            Trainee trainee = new Trainee("Слава", "", 3);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.TRAINEE_WRONG_LASTNAME, ex.getErrorCodes());
        }

        try {
            Trainee trainee = new Trainee("Влад", "Владович", 3);
            trainee.setLastName(null);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.TRAINEE_WRONG_LASTNAME, ex.getErrorCodes());
        }

        try {
            Trainee trainee = new Trainee("Лёня", "Леоньтьев", 5);
            trainee.setLastName("");
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.TRAINEE_WRONG_LASTNAME, ex.getErrorCodes());
        }
    }

    @SuppressWarnings("unused")
    @Test
    public void testWrongGrade() throws TraineeException {
        try {
            Trainee trainee = new Trainee("Витя", "Викторов", 0);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.TRAINEE_WRONG_GRADE, ex.getErrorCodes());
        }

        try {
            Trainee trainee = new Trainee("Витя", "Викторов", 6);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.TRAINEE_WRONG_GRADE, ex.getErrorCodes());
        }

        try {
            Trainee trainee = new Trainee("Витя", "Викторов", 2);
            trainee.setGrade(0);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.TRAINEE_WRONG_GRADE, ex.getErrorCodes());
        }

        try {
            Trainee trainee = new Trainee("Витя", "Викторов", 5);
            trainee.setGrade(6);
            fail();
        } catch (TraineeException ex) {
            assertEquals(TraineeErrorCodes.TRAINEE_WRONG_GRADE, ex.getErrorCodes());
        }
    }
}
