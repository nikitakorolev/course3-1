package ru.omsu.imit.course3.first;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestServer {
    @Test
    public void test() {
//        Server server = new Server();
//        String json = "{firstName: \"test\", lastName: \"test2\", age: 5}";
//        server.add(json);
//        JsonElement jsonElement = new JsonParser().parse(json);
//        JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
//        JsonElement test213 = jsonObject.get("age");
//
//        String test = server.get("{\"age\": 4}");

        Client client = new Client();

        for (int i =0; i < 10; i++) {
            client.add(new Person("Тест персон" + i, "LastName", i));
        }

        assertEquals(15, client.size());

        for (int i = 0; i < 3; i++) {
            assertEquals(new Person("Имя " + i, "Фамилия " + i, i),client.get("{\"age\":" + i + "}"));
        }

        assertEquals(new Person(null, null, 0),client.get("{\"firstName\":\"name\"}"));
        assertEquals(new Person("Тест персон1", "LastName", 1),client.get("{\"firstName\":\"Тест персон1\", \"lastName\": \"LastName\"}"));
    }
}
