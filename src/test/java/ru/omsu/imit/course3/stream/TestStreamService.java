package ru.omsu.imit.course3.stream;

import org.junit.Test;
import ru.omsu.imit.course3.trainee.Trainee;
import ru.omsu.imit.course3.trainee.TraineeException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.junit.Assert.*;

public class TestStreamService {
    @Test
    public void testStreamSerializeDeserializeTraineeBinary() throws TraineeException, ClassNotFoundException {
        Trainee traineeToWrite = new Trainee("Акакий", "Акакевич", 5);
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            StreamService.serializeTraineeToByteArrayOutputStream(baos, traineeToWrite);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            Trainee traineeRead = StreamService.deserializeTraineeFromByteArrayInputStream(bais);
            assertEquals(traineeToWrite, traineeRead);
        } catch (IOException ex) {
            fail();
        }
    }
}
