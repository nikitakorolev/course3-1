package ru.omsu.imit.course3.java8;

import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static ru.omsu.imit.course3.java8.Stream.*;

public class TestStream {
    @Test
    public void testTransform1() {
        IntStream stream = IntStream.of(1, 10, 2, 20);
        IntUnaryOperator op = (x) -> x + 256;
        transform1(stream, op).forEach(System.out::println);
    }

    @Test
    public void testTransform2() {
        IntStream stream = IntStream.of(100, 200, 300, 124, 16, 12516);
        IntUnaryOperator op = (x) -> x % 2;
        transform1(stream, op).forEach(System.out::println);
    }

    @Test
    public void testGetUniqueNameSortByNameLength() {
        List<Person> personList = new ArrayList<>();
        Set<String> names = new LinkedHashSet<>();

        Person person1 = new Person("Саша", 14);
        Person person2 = new Person("Семён", 25);
        Person person3 = new Person("Сергей", 79);
        Person person4 = new Person("Гоша", 30);
        Person person5 = new Person("Гриша", 31);
        Person person6 = new Person("Сергей", 60);

        personList.add(person1);
        personList.add(person2);
        personList.add(person3);
        personList.add(person4);
        personList.add(person5);
        personList.add(person6);

        names.add("Сергей");
        names.add("Гриша");

        assertEquals(names, getUniqueNamesSortByNameLength.apply(personList));
    }

    @Test
    public void testGetUniqueNamesSortBySameName() {
        List<Person> personList = new ArrayList<>();
        Set<String> names = new LinkedHashSet<>();

        Person person1 = new Person("Семён", 67);
        Person person2 = new Person("Семён", 45);
        Person person3 = new Person("Сергей", 79);
        Person person4 = new Person("Семён", 31);
        Person person5 = new Person("Гриша", 31);
        Person person6 = new Person("Сергей", 60);

        personList.add(person1);
        personList.add(person2);
        personList.add(person3);
        personList.add(person4);
        personList.add(person5);
        personList.add(person6);

        names.add("Семён");
        names.add("Сергей");
        names.add("Гриша");

        assertEquals(names, getUniqueNamesSortBySameName.apply(personList));
    }

    @Test
    public void testSum() {
        List<Integer> integerList = new ArrayList<>();
        integerList.add(5);
        integerList.add(10);
        integerList.add(-12);
        integerList.add(2);

        assertEquals(Integer.valueOf(5), sum.apply(integerList));
    }

    @Test
    public void testProduct() {
        List<Integer> integerList = new ArrayList<>();
        integerList.add(5);
        integerList.add(10);
        integerList.add(-12);
        integerList.add(2);

        assertEquals(Integer.valueOf(-1200), product.apply(integerList));
    }
}
