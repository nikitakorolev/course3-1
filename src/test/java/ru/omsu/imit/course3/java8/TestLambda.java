package ru.omsu.imit.course3.java8;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static ru.omsu.imit.course3.java8.Lambda.*;

public class TestLambda {
    @Test
    public void testSplitCount() {
        String s = "съешь ещё этих мягких французских булок, да выпей чаю";
        List<String> stringList = split.apply(s);
        Integer count1 = splitAndCount1.apply(s);
        Integer count2 = splitAndCount2.apply(s);

        List<String> expected = Arrays.asList("съешь", "ещё", "этих", "мягких", "французских", "булок,", "да", "выпей", "чаю");

        assertEquals(expected, stringList);
        assertEquals(9, count.apply(stringList).intValue());
        assertEquals(count1, count.apply(stringList));
        assertEquals(count1, count2);
    }

    @Test
    public void testCreate() {
        // Person person = new Person("Сергей");
        assertEquals(new Person("Сергей"), create.apply("Сергей"));
    }

    @Test
    public void testMax() {
        assertEquals(124, max.apply(18, 124).intValue());
    }

    @Test
    public void testIsEven() {
        assertTrue(isEven.test(128));
        assertFalse(isEven.test(129));
    }

    @Test
    public void testAreEquals() {
        assertTrue(areEqual.test(256, 256));
        assertFalse(areEqual.test(128, 1024));
    }

    @Test
    public void testGetMothersMotherFather1() {
        Person2 greatGrandfather = new Person2(null, null);
        Person2 grandmother = new Person2(greatGrandfather, null);
        Person2 mother = new Person2(null, grandmother);
        Person2 person = new Person2(null, mother);

        Person2 grandmotherWithoutFather = new Person2(null, null);
        Person2 motherWithoutGrandfather = new Person2(null, grandmotherWithoutFather);
        Person2 person2 = new Person2(null, motherWithoutGrandfather);

        assertEquals(greatGrandfather, getMothersMotherFather1.apply(person));
        assertNull(getMothersMotherFather1.apply(person2));
    }

    @Test
    public void testGetMothersMotherFather2() {
        OptionalPerson greatGrandfather = new OptionalPerson(null, null);
        OptionalPerson grandmother = new OptionalPerson(greatGrandfather, null);
        OptionalPerson mother = new OptionalPerson(null, grandmother);
        OptionalPerson person = new OptionalPerson(null, mother);

        OptionalPerson grandmotherWithoutFather = new OptionalPerson(null, null);
        OptionalPerson motherWithoutGrandfather = new OptionalPerson(null, grandmotherWithoutFather);
        OptionalPerson person2 = new OptionalPerson(null, motherWithoutGrandfather);

        assertEquals(Optional.of(greatGrandfather), getMothersMotherFather2.apply(person));
        assertEquals(Optional.empty(), getMothersMotherFather2.apply(person2));
    }
}
