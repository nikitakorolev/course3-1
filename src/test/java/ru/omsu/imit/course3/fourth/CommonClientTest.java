package ru.omsu.imit.course3.fourth;

import org.junit.Test;
import ru.omsu.imit.course3.fourth.rest.response.FailureResponse;
import ru.omsu.imit.course3.fourth.utils.ErrorCode;

public class CommonClientTest extends BaseClientTest {
    @Test
    public void testWrongUrl() {
        Object response = client.post(getBaseURL() + "/wrong", null, FailureResponse.class);
        checkFailureResponse((FailureResponse) response, ErrorCode.WRONG_URL);
    }

    @Test
    public void testWrongJson() {
        Object response = client.postWrongJson(getBaseURL() + "/person", "test: ", FailureResponse.class);
        checkFailureResponse((FailureResponse) response, ErrorCode.JSON_PARSE_EXCEPTION);
    }

    @Test
    public void testEmptyJson() {
        Object response = client.postWrongJson(getBaseURL() + "/person", "", FailureResponse.class);
        checkFailureResponse((FailureResponse) response, ErrorCode.NULL_REQUEST);
    }
}
