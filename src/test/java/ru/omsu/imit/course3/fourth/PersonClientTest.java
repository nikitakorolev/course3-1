package ru.omsu.imit.course3.fourth;

import org.junit.Test;
import ru.omsu.imit.course3.fourth.model.Person;
import ru.omsu.imit.course3.fourth.rest.response.PersonInfoResponse;
import ru.omsu.imit.course3.fourth.utils.ErrorCode;

public class PersonClientTest extends BaseClientTest {
    @Test
    public void testAddPerson() {
        addPerson(new Person("Сергей","Булочкин", 22), ErrorCode.SUCCESS);
    }

    @Test
    public void testGetPerson() {
        PersonInfoResponse addResponse = addPerson(new Person("Сергей","Булочкин", 22), ErrorCode.SUCCESS);
        getPerson(new Person("Сергей", null, null), ErrorCode.SUCCESS);
    }
}
