package ru.omsu.imit.course3.fourth;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.course3.fourth.client.PersonClient;
import ru.omsu.imit.course3.fourth.model.Person;
import ru.omsu.imit.course3.fourth.rest.request.PersonRequest;
import ru.omsu.imit.course3.fourth.rest.response.FailureResponse;
import ru.omsu.imit.course3.fourth.rest.response.PersonInfoResponse;
import ru.omsu.imit.course3.fourth.server.PersonServer;
import ru.omsu.imit.course3.fourth.server.config.Settings;
import ru.omsu.imit.course3.fourth.utils.ErrorCode;

import java.net.Inet4Address;
import java.net.UnknownHostException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BaseClientTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseClientTest.class);

    protected static PersonClient client = new PersonClient();
    private static String baseURL;

    private static void initialize() {
        String hostName = null;
        try {
            hostName = Inet4Address.getLocalHost().getCanonicalHostName();
        } catch (UnknownHostException ex) {
            LOGGER.debug("Can't determine my own host name", ex);
        }
        baseURL = "http://" + hostName + ":" + Settings.getRestHttpPort() + "/api";
    }

    @BeforeClass
    public static void startServer() {
        initialize();
        PersonServer.createServer();
    }

    @AfterClass
    public static void stopServer() {
        PersonServer.stopServer();
    }

    public static String getBaseURL() {
        return baseURL;
    }

    protected void checkFailureResponse(Object response, ErrorCode expectedStatus) {
        assertTrue(response instanceof FailureResponse);
        FailureResponse failureResponseObject = (FailureResponse) response;
        assertEquals(expectedStatus, failureResponseObject.getErrorCode());
    }

    protected PersonInfoResponse addPerson(Person person, ErrorCode expectedStatus) {
        PersonRequest request = new PersonRequest(person);
        Object response = client.post(baseURL + "/person", request, PersonInfoResponse.class);
        if (response instanceof PersonInfoResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            PersonInfoResponse addPersonInfoResponse = (PersonInfoResponse) response;
            assertEquals(new PersonInfoResponse(person), addPersonInfoResponse);
            return addPersonInfoResponse;
        }
        else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }

    protected PersonInfoResponse getPerson(Person person, ErrorCode expectedStatus) {
        String request = "firstName=" + person.getFirstName() +
                         "&lastName=" +
                         "&age=";
        Object response = client.get(baseURL + "/person?" + request, PersonInfoResponse.class);
        if (response instanceof PersonInfoResponse) {
            assertEquals(ErrorCode.SUCCESS, expectedStatus);
            PersonInfoResponse getPersonInfoResponse = (PersonInfoResponse) response;
            //assertEquals(new PersonInfoResponse(person), getPersonInfoResponse);
            return getPersonInfoResponse;
        }
        else {
            checkFailureResponse(response, expectedStatus);
            return null;
        }
    }
}
