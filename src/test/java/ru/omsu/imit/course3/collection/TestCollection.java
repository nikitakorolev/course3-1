package ru.omsu.imit.course3.collection;

import org.junit.Test;
import ru.omsu.imit.course3.trainee.Institute;
import ru.omsu.imit.course3.trainee.Trainee;
import ru.omsu.imit.course3.trainee.TraineeException;
import ru.omsu.imit.course3.trainee.TreeSetTrainee;

import java.util.*;

import static org.junit.Assert.*;

public class TestCollection {
    @Test
    public void testAccessSpeedArrayListLinkedList() {
        ArrayList<Integer> numArrayList = new ArrayList<>();
        for (int i = 0; i < 99999; i++) {
            numArrayList.add(i);
        }
        LinkedList<Integer> numLinkedList = new LinkedList<>();
        for (int i = 0; i < 99999; i++) {
            numLinkedList.add(i);
        }

        Random random = new Random();
        int rand = random.nextInt(99999);
        long timeBefore = System.nanoTime();
        for (int i = 0; i < 99999; i++) {
            numArrayList.get(rand);
        }
        System.out.println("ArrayList: " + (System.nanoTime() - timeBefore));

        timeBefore = System.nanoTime();
        for (int i = 0; i < 99999; i++) {
            numLinkedList.get(rand);
        }
        System.out.println("LinkedList: " + (System.nanoTime() - timeBefore));
    }

    @Test
    public void testTraineeQueue() throws TraineeException {
        Queue<Trainee> trainees = new LinkedList<>();
        Trainee trainee1 = new Trainee("Жорик", "Жорикович", 3);
        Trainee trainee2 = new Trainee("Кирилл", "Кириллович", 4);
        Trainee trainee3 = new Trainee("Никита", "Никитич", 2);
        assertTrue(trainees.offer(trainee1));
        assertTrue(trainees.offer(trainee2));
        assertTrue(trainees.offer(trainee3));
        assertEquals(trainee1, trainees.poll());
        assertEquals(trainee2, trainees.poll());
        assertEquals(trainee3, trainees.poll());
        assertEquals(0, trainees.size());
    }

    @Test
    public void testTraineeHashSet() throws TraineeException {
        Set<Trainee> trainees = new HashSet<>();
        Trainee trainee1 = new Trainee("Жорик", "Жорикович", 3);
        Trainee trainee2 = new Trainee("Кирилл", "Кириллович", 4);
        trainees.add(trainee1);
        trainees.add(trainee2);
        assertTrue(trainees.contains(trainee1));
        System.out.println(trainees.toString());
    }

    @Test
    public void testTraineeTreeSet() throws TraineeException {
        Set<Trainee> trainees = new TreeSet<>();
        Trainee trainee1 = new Trainee("Жорик", "Жорикович", 3);
        Trainee trainee2 = new Trainee("Кирилл", "Кириллович", 4);
        trainees.add(trainee1);
        trainees.add(trainee2);
        assertTrue(trainees.contains(trainee2));
        System.out.println(trainees.toString());
    }

    @Test
    public void testTraineeTreeSet2() throws TraineeException {
        TreeSetTrainee treeSetTrainee = new TreeSetTrainee();
        Trainee trainee1 = new Trainee("Жорик", "Жорикович", 3);
        Trainee trainee2 = new Trainee("Жорик", "Григорьевич", 3);
        Trainee trainee3 = new Trainee("Жорик", "Зааа", 3);
        treeSetTrainee.addTrainee(trainee1);
        treeSetTrainee.addTrainee(trainee2);
        assertTrue(treeSetTrainee.haveTraineeWithDifferentLastName(trainee3));
        System.out.println(treeSetTrainee.getTrainees().toString());
    }

    @Test
    public void testSearchSpeedArrayListSet() {
        List<Integer> numArrayList = new ArrayList<>();
        Set<Integer> numHashSet = new HashSet<>();
        Set<Integer> numTreeSet = new TreeSet<>();

        Random random = new Random();
        for (int i = 0; i < 100000; i++) {
            numArrayList.add(i);
            numHashSet.add(i);
            numTreeSet.add(i);
        }

        assertEquals(100000, numArrayList.size());
        assertEquals(100000, numHashSet.size());
        assertEquals(100000, numTreeSet.size());


        int rand = random.nextInt(100000);
        long timeBefore = System.nanoTime();
        for (int i = 0; i < 10000; i++) {
            numArrayList.contains(rand);
        }
        System.out.println("ArrayList: " + (System.nanoTime() - timeBefore));

        timeBefore = System.nanoTime();
        for (int i = 0; i < 10000; i++) {
            numHashSet.contains(rand);
        }
        System.out.println("HashSet: " + (System.nanoTime() - timeBefore));

        timeBefore = System.nanoTime();
        for (int i = 0; i < 10000; i++) {
            numTreeSet.contains(rand);
        }
        System.out.println("TreeSet: " + (System.nanoTime() - timeBefore));
    }

    @Test
    public void testHashMapTraineeInstitute() throws TraineeException {
        Map<Trainee, Institute> hashMap = new HashMap<>();
        Trainee trainee1 = new Trainee("Иван", "Иванов", 1);
        Trainee trainee2 = new Trainee("Федор", "Фёдоров", 2);
        Trainee trainee3 = new Trainee("Петр", "Петров", 4);
        Trainee trainee4 = new Trainee("Александр", "Александрович", 5);
        Institute institute1 = new Institute("ОмГУ", "Омск");
        Institute institute2 = new Institute("НГУ", "Новосибирск");

        hashMap.put(trainee1, institute1);
        hashMap.put(trainee2, institute1);
        hashMap.put(trainee3, institute2);
        hashMap.put(trainee4, institute2);

        assertEquals(4, hashMap.size());
        assertEquals(institute1, hashMap.get(trainee2));

        System.out.println(hashMap.keySet().toString());
    }

    @Test
    public void testTreeMapTraineeInstitute() throws TraineeException {
        Map<Trainee, Institute> treeMap = new TreeMap<>();
        Trainee trainee1 = new Trainee("Иван", "Иванов", 1);
        Trainee trainee2 = new Trainee("Федор", "Фёдоров", 2);
        Trainee trainee3 = new Trainee("Петр", "Петров", 4);
        Trainee trainee4 = new Trainee("Александр", "Александрович", 5);
        Institute institute1 = new Institute("ОмГУ", "Омск");
        Institute institute2 = new Institute("НГУ", "Новосибирск");

        treeMap.put(trainee1, institute1);
        treeMap.put(trainee2, institute1);
        treeMap.put(trainee3, institute2);
        treeMap.put(trainee4, institute2);

        assertEquals(4, treeMap.size());

        System.out.println(treeMap.keySet().toString());
        System.out.println(treeMap.entrySet().toString());
    }

    @Test
    public void testTreeMapTraineeInstitute2() throws TraineeException {
        Map<Trainee, Institute> treeMap = new TreeMap<>((t1, t2) -> t1.getFullName().compareTo(t2.getFullName()));
        Trainee trainee1 = new Trainee("Иван", "Иванов", 1);
        Trainee trainee2 = new Trainee("Федор", "Фёдоров", 2);
        Trainee trainee3 = new Trainee("Петр", "Петров", 4);
        Trainee trainee4 = new Trainee("Александр", "Александрович", 5);
        Trainee trainee5 = new Trainee("Петр", "Афанасьевич", 4);
        Institute institute1 = new Institute("ОмГУ", "Омск");
        Institute institute2 = new Institute("НГУ", "Новосибирск");

        treeMap.put(trainee1, institute1);
        treeMap.put(trainee2, institute1);
        treeMap.put(trainee3, institute2);
        treeMap.put(trainee4, institute2);

        assertEquals(4, treeMap.size());
        assertEquals(null, treeMap.get(trainee5));

        System.out.println(treeMap.keySet().toString());
        System.out.println(treeMap.entrySet().toString());
    }

    @Test
    public void testBitSet() {
        BitSet bitSet = new BitSet();
        bitSet.set(1);
        bitSet.set(2);
        bitSet.set(3);
        bitSet.set(15);
        bitSet.set(20);

        assertEquals(21, bitSet.length());
        assertTrue(bitSet.get(15));
        bitSet.set(2, false);
        assertFalse(bitSet.get(2));
    }

    @Test
    public void testAddSpeedBitHashTreeSet() {
        BitSet bitSet = new BitSet();
        HashSet<Integer> hashSet = new HashSet<>();
        TreeSet<Integer> treeSet = new TreeSet<>();

        long timeBefore = System.nanoTime();
        for (int i = 0; i < 100000; i++) {
            bitSet.set(i);
        }
        System.out.println("BitSet: " + (System.nanoTime() - timeBefore));

        timeBefore = System.nanoTime();
        for (int i = 0; i < 100000; i++) {
            hashSet.add(i);
        }
        System.out.println("HashSet: " + (System.nanoTime() - timeBefore));

        timeBefore = System.nanoTime();
        for (int i = 0; i < 100000; i++) {
            treeSet.add(i);
        }
        System.out.println("TreeSet: " + (System.nanoTime() - timeBefore));
    }
}
