package ru.omsu.imit.course3.third;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class TestClientServer {
    @Test
    public void testClientServer() throws IOException{
        Client client = new Client();

        Gson gson = new Gson();
        PersonRequest Filip = new PersonRequest("Филип", "Мышкин", 21);
        PersonRequest Sergey = new PersonRequest("Сергей", "Снегирёв", 22);

        String addResp = client.sendMessage("add " + gson.toJson(Filip, PersonRequest.class));
        String wrongAddResp = client.sendMessage("add {awdsadfv:\"28ljsv\":.?///,,,as}");
        String getResp = client.sendMessage("get {firstName:Сергей}");
        String wrongGetResp = client.sendMessage("get {ASdAsc::>//.,.345gd)");
        String wrongResp = client.sendMessage("delete{}asac");

        assertEquals(gson.toJson(Filip, PersonRequest.class), addResp);
        assertEquals("Wrong json", wrongAddResp);
        assertEquals(gson.toJson(Sergey, PersonRequest.class), getResp);
        assertEquals("Wrong json", wrongGetResp);
        assertEquals("Wrong request", wrongResp);
    }

}
