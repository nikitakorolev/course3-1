package ru.omsu.imit.course3.inputoutput;

import ru.omsu.imit.course3.trainee.Trainee;
import ru.omsu.imit.course3.trainee.TraineeException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.EnumSet;

public class NewInputOutput {
    public static Trainee readTraineeFromFileByteBuffer(File file) throws IOException, TraineeException {
        try (SeekableByteChannel sbc = Files.newByteChannel(Paths.get(file.getPath()))) {
            long fileSize = sbc.size();
            ByteBuffer buf = ByteBuffer.allocate((int) fileSize);

            String encoding = System.getProperty("file.encoding");

            while (sbc.read(buf) > 0) {
                buf.flip();
            }

            String oneLine = Charset.forName(encoding).decode(buf).toString();

            String[] data = oneLine.split(" ");

            return new Trainee(data[0], data[1], Integer.valueOf(data[2]));
        } catch (NumberFormatException ex) {
            throw new NumberFormatException();
        }
    }

    public static Trainee readTraineeFromFileMappedByteBuffer(File file) throws IOException, TraineeException {
        try (FileChannel fc = (FileChannel) Files.newByteChannel(Paths.get(file.getPath()))) {
            long fileSize = fc.size();
            MappedByteBuffer buf = fc.map(FileChannel.MapMode.READ_ONLY, 0, fileSize);

            String encoding = System.getProperty("file.encoding");

            String oneLine = Charset.forName(encoding).decode(buf).toString();

            String[] data = oneLine.split(" ");

            return new Trainee(data[0], data[1], Integer.valueOf(data[2]));
        } catch (NumberFormatException ex) {
            throw new NumberFormatException();
        }
    }

    public static void writeNumArrayToFileMappedByteBuffer(File file, int[] num) throws IOException {
        try (FileChannel fc = (FileChannel) Files
                .newByteChannel(Paths.get(file.getPath()), EnumSet.of(
                        StandardOpenOption.READ,
                        StandardOpenOption.WRITE,
                        StandardOpenOption.TRUNCATE_EXISTING))) {

            ByteBuffer byteBuffer = ByteBuffer.allocate(num.length * 4);
            byteBuffer.asIntBuffer().put(num);

            MappedByteBuffer buf = fc.map(FileChannel.MapMode.READ_WRITE, 0, num.length * 4);

            if (buf != null) {
                buf.put(byteBuffer);
            }
        }
    }

    public static ByteBuffer serializeTraineeToByteByffer(Trainee trainee) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(trainee);
            return ByteBuffer.wrap(baos.toByteArray());
        }
    }
}
