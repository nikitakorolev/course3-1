package ru.omsu.imit.course3.color;

public enum ColorErrorCode {
    WRONG_COLOR_STRING("Wrong color"),
    NULL_COLOR("Cannot applied to null");

    String errorString;

    ColorErrorCode(String errorString) {
        this.errorString = errorString;
    }

    public String getErrorString() {
        return errorString;
    }
}
