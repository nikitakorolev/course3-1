package ru.omsu.imit.course3.color;

public class ColorException extends Exception {
    private ColorErrorCode colorErrorCode;

    public ColorException(ColorErrorCode colorErrorCode) {
        this.colorErrorCode = colorErrorCode;
    }

    public ColorErrorCode getErrorCode() {
        return colorErrorCode;
    }
}
