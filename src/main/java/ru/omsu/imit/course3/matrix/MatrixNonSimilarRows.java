package ru.omsu.imit.course3.matrix;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MatrixNonSimilarRows {
    private int[][] matrix;

    public MatrixNonSimilarRows(int[][] matrix) {
        this.matrix = matrix;
    }

    public Set<int[]> getNonSimilarRows() {
        Map<Set<Integer>, int[]> hashMap = new HashMap<>();
        for (int[] row : matrix) {
            Set<Integer> rowSet = new HashSet<>();
            for (int elem : row) {
                rowSet.add(elem);
            }
            hashMap.putIfAbsent(rowSet, row);
        }
        return new HashSet<>(hashMap.values());
    }
}
