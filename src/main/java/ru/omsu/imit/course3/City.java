package ru.omsu.imit.course3;

import java.util.Objects;

public class City {
    private String nameOfCity;
    private int countOfPeople;
    private String typeOfCity;

    public City() {
        this.countOfPeople = 0;
        this.nameOfCity = "";
        this.typeOfCity = "";
    }

    public City(String name, int count, String type) {
        this.nameOfCity = name;
        this.countOfPeople = count;
        this.typeOfCity = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return countOfPeople == city.countOfPeople &&
                Objects.equals(nameOfCity, city.nameOfCity) &&
                Objects.equals(typeOfCity, city.typeOfCity);
    }
}