package ru.omsu.imit.course3.third;

import java.io.*;
import java.net.Socket;
import java.rmi.ServerException;
import java.util.Scanner;

public class Client {
    private static Socket clientSocket;
    private static BufferedReader in;
    private static BufferedWriter out;

    public Client() throws IOException {
        clientSocket = new Socket("localhost", 4004);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
    }

    public String sendMessage(String msg) throws IOException {
        out.write(msg + "\n");
        out.flush();
        return in.readLine();
    }

    public void close() throws IOException {
        out.flush();
        out.close();
        in.close();
        clientSocket.close();
    }

    public void closeServer() throws IOException {
        out.write("exit" + "\n");
    }

    public static void main(String[] args) throws IOException, ServerException {
        Scanner in = new Scanner(System.in);
        Client client = new Client();
        while (true) {
            System.out.println("Enter method");
            String line = in.nextLine();
            if (line.equals("exit")) {
                client.closeServer();
                break;
            }
            System.out.println(client.sendMessage(line));
        }
        client.closeServer();
        client.close();
    }
}
