package ru.omsu.imit.course3.third;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.List;

public class Server {
    private List<Person> personList;
    private static Socket clientSocket;
    private static ServerSocket server;
    private static BufferedReader in;
    private static BufferedWriter out;

    public Server() throws IOException, ServerException {
        server = new ServerSocket(4004);
        personList = new ArrayList<>();
        personList.add(new Person("Андрей", "Андреев", 15));
        personList.add(new Person("Сергей", "Снегирёв", 22));
        personList.add(new Person("Игорь", "Леший", 56));
    }

    public void getRequest() throws IOException, ServerException {
        System.out.println("Server wait request");
        clientSocket = server.accept();
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
        String res;
        while ((res = in.readLine()) != null) {
            System.out.println(res);
            if (res.equals("exit")) {
                close();
                break;
            }
            int space = res.indexOf(' ');
            if (space < 0) {
                out.write("Wrong request" + "\n");
                out.flush();
            }
            String method = res.substring(0, space);
            String json = res.substring(space + 1);
            System.out.println(json);
            if (method.equals("get")) {
                get(json);
            }
            if (method.equals("add")) {
                add(json);
            }
        }
    }

    public void get(String request) throws IOException, ServerException {
        String res = "{}";
        PersonRequest p = null;
        Gson gson = new Gson();
        try {
            p = gson.fromJson(request, PersonRequest.class);
        } catch (JsonSyntaxException ex) {
            out.write("Wrong json" + "\n");
            out.flush();
            ex.printStackTrace();
            return;
        }

        for (Person person : personList) {
            if (p.compare(person)) {
                res = gson.toJson(person);
                break;
            }
        }
        out.write(res + "\n");
        out.flush();
    }

    public void add(String request) throws IOException, ServerException {
        PersonRequest p = null;
        Gson gson = new Gson();
        try {
            p = gson.fromJson(request, PersonRequest.class);
        } catch (JsonSyntaxException ex) {
            out.write("Wrong json" + "\n");
            out.flush();
            ex.printStackTrace();
            return;
        }
        if (p.getFirstName() != null && p.getLastName() != null && p.getAge() != null) {
            personList.add(new Person(p));
            out.write(gson.toJson(p) + "\n");
            out.flush();
        }
    }

    public void close() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
        server.close();
    }

    public static void main(String[] args) throws IOException, ServerException {
        Server server = new Server();
        server.getRequest();
    }
}

