package ru.omsu.imit.course3.third;

public class PersonRequest {
    private String firstName;
    private String lastName;
    private Integer age;

    public PersonRequest(String firstName, String lastName, Integer age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getAge() {
        return age;
    }

    public boolean compare(Person person) {
        return ((firstName == null || firstName.equals(person.getFirstName())) &&
                (lastName == null || lastName.equals(person.getLastName())) &&
                (age == null || age.equals(person.getAge())) &&
                (firstName != null || lastName != null || age != null));
    }
}
