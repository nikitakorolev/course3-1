package ru.omsu.imit.course3.java8;

import java.util.Objects;

public class Person2 {
    private Person2 father;
    private Person2 mother;

    public Person2(Person2 father, Person2 mother) {
        this.father = father;
        this.mother = mother;
    }

    public Person2 getFather() {
        return father;
    }

    public Person2 getMother() {
        return mother;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person2 person2 = (Person2) o;
        return Objects.equals(father, person2.father) &&
                Objects.equals(mother, person2.mother);
    }

    @Override
    public int hashCode() {

        return Objects.hash(father, mother);
    }
}
