package ru.omsu.imit.course3.java8;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.*;

public class Lambda {
    public static final Function<String, List<String>> split = (s) -> Arrays.asList(s.split(" "));

    public static final Function<List<?>, Integer> count = List::size;

    public static final Function<String, Integer> splitAndCount1 = (s) -> split.andThen(count).apply(s);

    public static final Function<String, Integer> splitAndCount2 = (s) -> count.compose(split).apply(s);

    public static final Function<String, Person> create = Person::new;

    public static final BinaryOperator<Integer> max = Math::max;

    public static final Supplier getCurrentDate = LocalDate::now;

    public static final Predicate<Integer> isEven = (a) -> a % 2 == 0;

    public static final BiPredicate<Integer, Integer> areEqual = Integer::equals;

    public static final UnaryOperator<Person2> getMothersMotherFather1 = (person) -> {
        if (person.getMother() != null && person.getMother().getMother() != null) {
            return person.getMother().getMother().getFather();
        }
        return null;
    };

    public static final Function<OptionalPerson, Optional<OptionalPerson>> getMothersMotherFather2 = (person) ->
            Optional.of(person)
                    .flatMap(OptionalPerson::getMother)
                    .flatMap(OptionalPerson::getMother)
                    .flatMap(OptionalPerson::getFather);
}
