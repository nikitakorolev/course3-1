package ru.omsu.imit.course3.java8;

import java.util.Objects;
import java.util.Optional;

public class OptionalPerson {
    private Optional<OptionalPerson> father;
    private Optional<OptionalPerson> mother;

    public OptionalPerson(OptionalPerson father, OptionalPerson mother) {
        this.father = Optional.ofNullable(father);
        this.mother = Optional.ofNullable(mother);
    }

    public Optional<OptionalPerson> getFather() {
        return father;
    }

    public Optional<OptionalPerson> getMother() {
        return mother;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OptionalPerson that = (OptionalPerson) o;
        return Objects.equals(father, that.father) &&
                Objects.equals(mother, that.mother);
    }

    @Override
    public int hashCode() {

        return Objects.hash(father, mother);
    }
}
