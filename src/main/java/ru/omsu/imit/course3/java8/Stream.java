package ru.omsu.imit.course3.java8;

import java.util.*;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Stream {
    public static final IntStream transform1(IntStream stream, IntUnaryOperator op) {
        return stream.map(op);
    }

    public static final IntStream transform2(IntStream stream, IntUnaryOperator op) {
        return stream.parallel().map(op);
    }

    public static final Function<List<Person>, Set<String>> getUniqueNamesSortByNameLength = (list) ->
            list.stream()
                    .filter((p) -> p.getAge() > 30)
                    .map(Person::getName)
                    .sorted(Comparator.comparing(String::length))
                    .collect(Collectors.toCollection(LinkedHashSet::new));

    public static final Function<List<Person>, Set<String>> getUniqueNamesSortBySameName = (list) ->
            list.stream()
                    .filter((p) -> p.getAge() > 30)
                    .collect(Collectors.groupingBy(Person::getName, Collectors.counting()))
                    .entrySet()
                    .stream()
                    .sorted(Map.Entry.<String, Long>comparingByValue().reversed())
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toSet());

    public static final Function<List<Integer>, Integer> sum = (list) ->
            list.stream()
                    .reduce(0, (a, b) -> a + b);

    public static final Function<List<Integer>, Integer> product = (list) ->
            list.stream().reduce(1, (a, b) -> a * b);
}
