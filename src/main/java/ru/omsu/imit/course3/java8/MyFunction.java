package ru.omsu.imit.course3.java8;

@FunctionalInterface
public interface MyFunction {
    <T> T apply(T t);
}
