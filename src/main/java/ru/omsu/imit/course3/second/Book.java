package ru.omsu.imit.course3.second;

import java.sql.Date;

public class Book {
    private int id;
    private String title;
    private Date year_of_issue;
    private int pages;

    public Book(int id, String title, Date year_of_issue, int pages) {
        super();
        this.id = id;
        this.title = title;
        this.year_of_issue = year_of_issue;
        this.pages = pages;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getYear_of_issue() {
        return year_of_issue;
    }

    public void setYear_of_issue(Date year_of_issue) {
        this.year_of_issue = year_of_issue;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", year_of_issue=" + year_of_issue +
                ", pages=" + pages +
                '}';
    }
}
