package ru.omsu.imit.course3.second;

import java.sql.*;

public class JDBC {
    private static final String URL = "jdbc:mysql://localhost:3306/bookstore?serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PASSWORD = "admin";

    private static boolean loadDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return true;
        } catch (ClassNotFoundException ex) {
            System.out.println("Error loading JDBC Driver");
            ex.printStackTrace();
            return false;
        }
    }

    private static void getBookByColNames(Connection con, String query) {
        System.out.println("Book: ");
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(query)) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                Date year_of_issue = rs.getDate("year_of_issue");
                int pages = rs.getInt("pages");
                Book book = new Book(id, title, year_of_issue, pages);
                System.out.println(book);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        System.out.println("End Book");
    }

    private static void insertBook(Connection con, String query) {
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            if (result > 0) {
                System.out.println("Result = " + result);
                ResultSet rs = stmt.getGeneratedKeys();
                if (rs.next()) {
                    int id = rs.getInt(1);
                    System.out.println("id = " + id);
                    rs.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private static void updateBook(Connection con, String query) {
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(query);
            System.out.println("Result = " + result);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private static void deleteBook(Connection con, String query) {
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(query);
            System.out.println("Result = " + result);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private static void deleteBooksInjection(Connection con, String query) {
        try (PreparedStatement stmt = con.prepareStatement(query)) {
            stmt.setString(1, "1984");
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) throws SQLException {
        if (!loadDriver()) {
            return;
        }

        Connection con = DriverManager.getConnection(URL, USER, PASSWORD);

        String selectQuery = "SELECT * FROM books";
        getBookByColNames(con, selectQuery);
        selectQuery = "SELECT * FROM books LIMIT 7";
        getBookByColNames(con, selectQuery);
        selectQuery = "SELECT * FROM books WHERE id = 3";
        getBookByColNames(con, selectQuery);
        selectQuery = "SELECT * FROM books WHERE title = \"1984\"";
        getBookByColNames(con, selectQuery);
        selectQuery = "SELECT * FROM books WHERE title LIKE \"Над%\"";
        getBookByColNames(con, selectQuery);
        selectQuery = "SELECT * FROM books WHERE title LIKE \"%лес\"";
        getBookByColNames(con, selectQuery);
        selectQuery = "SELECT * FROM books WHERE year_of_issue < \"2015\" AND year_of_issue > \"2000\"";
        getBookByColNames(con, selectQuery);
        selectQuery = "SELECT * FROM books WHERE pages > 300 AND pages < 600";
        getBookByColNames(con, selectQuery);
        selectQuery = "SELECT * FROM books WHERE (pages > 300 AND pages < 600) AND (year_of_issue < \"2015\" AND year_of_issue > \"2000\")";
        getBookByColNames(con, selectQuery);


        String updateQuery = "UPDATE books SET pages = 300";
        updateBook(con, updateQuery);
        updateQuery = "UPDATE books SET pages = 123 WHERE pages > 900";
        updateBook(con, updateQuery);
        updateQuery = "UPDATE books SET pages = 55 WHERE pages > 70 AND year_oF_issue > \"2014\"";
        updateBook(con, updateQuery);
        updateQuery = "UPDATE books SET pages = 500 WHERE pages > 200 AND title LIKE \"Над%\"";
        updateBook(con, updateQuery);


        String deleteQuery = "DELETE FROM books WHERE pages > 900";
        deleteBook(con, deleteQuery);
        deleteQuery = "DELETE FROM books WHERE title LIKE \"Над%\"";
        deleteBook(con, deleteQuery);
        deleteQuery = "DELETE FROM books WHERE id > 0 AND id < 3";
        deleteBook(con, deleteQuery);
        deleteQuery = "DELETE FROM books WHERE title = \"Норвежский лес\"";
        deleteBook(con, deleteQuery);
        deleteQuery = "DELETE FROM books WHERE title = \"1984\" OR title = \"Алмазы Эсмальди\" OR title = \"Запретный лес\"";
        deleteBook(con, deleteQuery);
        deleteQuery = "DELETE FROM books;";
        deleteBook(con, deleteQuery);

        String query = "ALTER TABLE books ADD COLUMN publishing_house VARCHAR (60)";
        updateBook(con, query);
        query = "CREATE TABLE bindings  (\n" +
                "\tbinding VARCHAR (15) NOT NULL,\n" +
                "    PRIMARY KEY (binding)\n" +
                ")";
        updateBook(con, query);

        String insertQuery = "INSERT INTO bindings VALUES(\"Без переплета\")";
        insertBook(con, insertQuery);
        insertQuery = "INSERT INTO bindings VALUES(\"Мягкий\")";
        insertBook(con, insertQuery);
        insertQuery = "INSERT INTO bindings VALUES(\"Твердый\")";
        insertBook(con, insertQuery);


        query = "ALTER TABLE books ADD COLUMN binding VARCHAR (20)";
        updateBook(con, query);
        query = "UPDATE books SET binding = \"Без переплёта\"";
        updateBook(con, query);
        query = "ALTER TABLE books MODIFY COLUMN binding VARCHAR (15) NOT NULL, ADD CONSTRAINT bindings_binding_fk FOREIGN KEY (binding) REFERENCES bindings(binding)";
        updateBook(con, query);
        query = "UPDATE books SET publishing_house = \"Эксмо\" WHERE id < 8 AND id > 3";
        updateBook(con, query);
        query = "ALTER TABLE books CHANGE COLUMN title bookname VARCHAR (100) NOT NULL";
        updateBook(con, query);
        query = "ALTER TABLE books RENAME book_list";
        updateBook(con, query);
        query = "DROP TABLE book_list";
        updateBook(con, query);

        deleteQuery = "delete from books where title = ?";

    }
}
