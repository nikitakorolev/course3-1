package ru.omsu.imit.course3.trainee;

public enum TraineeErrorCodes {
    TRAINEE_WRONG_FIRSTNAME("Incorrect first name"),
    TRAINEE_WRONG_LASTNAME("Incorrect last name"),
    TRAINEE_WRONG_GRADE("Incorrect grade"),
    TRAINEE_NOT_FOUND("Trainee not found"),
    GROUP_WRONG_NAME("Incorrect group name"),
    GROUP_WRONG_TRAINEES("Incorrect trainees"),
    GROUP_NOT_FOUND("Group not found"),
    INSTITUTE_WRONG_NAME("Incorrect name"),
    INSTITUTE_WRONG_CITY("Incorrect city"),
    INSITUTE_NOT_FOUND("Institute not found");

    private String errorString;

    TraineeErrorCodes(String errorString) {
        this.errorString = errorString;
    }

    public String getErrorString() {
        return errorString;
    }
}
