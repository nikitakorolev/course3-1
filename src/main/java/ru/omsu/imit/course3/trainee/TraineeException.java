package ru.omsu.imit.course3.trainee;

public class TraineeException extends Exception {
    private TraineeErrorCodes traineeErrorCodes;

    public TraineeException(TraineeErrorCodes traineeErrorCodes) {
        this.traineeErrorCodes = traineeErrorCodes;
    }

    public TraineeErrorCodes getErrorCodes() {
        return traineeErrorCodes;
    }
}
