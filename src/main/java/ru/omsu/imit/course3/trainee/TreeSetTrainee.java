package ru.omsu.imit.course3.trainee;

import java.util.TreeSet;

public class TreeSetTrainee {
    private TreeSet<Trainee> trainees;

    public TreeSetTrainee() {
        this.trainees = new TreeSet<Trainee>();
    }

    public void addTrainee(Trainee trainee) {
        trainees.add(trainee);
    }

    public boolean haveTraineeWithDifferentLastName(Trainee trainee) {
        Trainee ceiling = trainees.ceiling(trainee);

        if (ceiling != null) {
            if (ceiling.getFirstName().equals(trainee.getFirstName()) && !ceiling.getLastName().equals(trainee.getLastName())) {
                return true;
            }
        }
        return false;
    }

    public TreeSet<Trainee> getTrainees() {
        return trainees;
    }
}
