package ru.omsu.imit.course3.trainee;

import org.apache.commons.lang3.StringUtils;

public class Institute {
    private String name;
    private String city;

    public Institute(String name, String city) throws TraineeException {
        setName(name);
        setCity(city);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws TraineeException {
        if (StringUtils.isEmpty(name)) {
            throw new TraineeException(TraineeErrorCodes.INSTITUTE_WRONG_NAME);
        }
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) throws TraineeException {
        if(StringUtils.isEmpty(city)) {
            throw new TraineeException(TraineeErrorCodes.INSTITUTE_WRONG_CITY);
        }
        this.city = city;
    }

    @Override
    public String toString() {
        return getCity() + ": " + getName();
    }
}
