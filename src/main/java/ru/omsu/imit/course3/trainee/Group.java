package ru.omsu.imit.course3.trainee;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class Group {
    private String name;
    private Trainee[] trainees;

    public Group(String name, Trainee[] trainees) throws TraineeException {
        setName(name);
        // trainees = new Trainee[size];
        setTrainees(trainees);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws TraineeException {
        if (StringUtils.isEmpty(name)) {
            throw new TraineeException(TraineeErrorCodes.GROUP_WRONG_NAME);
        }
        this.name = name;
    }

    public Trainee[] getTrainees() {
        return trainees;
    }

    public void setTrainees(Trainee[] trainees) throws TraineeException {
        if (ArrayUtils.isEmpty(trainees)) {
            throw new TraineeException(TraineeErrorCodes.GROUP_WRONG_TRAINEES);
        } else {
            for (Trainee trainee : trainees) {
                if (trainee == null) {
                    throw new TraineeException(TraineeErrorCodes.GROUP_WRONG_TRAINEES);
                }
            }
        }
        this.trainees = trainees;
    }

    public void sortTraineesByGrade() {
        Arrays.sort(trainees, (t1, t2) -> t2.getGrade() - t1.getGrade());
    }

    public void sortTraineesByFirstName() {
        Arrays.sort(trainees);
    }

    public Trainee getTraineeByFirstName(String firstName) throws TraineeException {
        for (Trainee trainee : trainees) {
            if (trainee.getFirstName().equals(firstName)) {
                return trainee;
            }
        }
        throw new TraineeException(TraineeErrorCodes.TRAINEE_NOT_FOUND);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(name, group.name) &&
                Objects.equals(trainees, group.trainees);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, trainees);
    }
}
