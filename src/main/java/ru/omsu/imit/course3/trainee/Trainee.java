package ru.omsu.imit.course3.trainee;

import java.io.Serializable;
import java.util.Objects;

public class Trainee implements Serializable, Comparable<Trainee> {
    private String firstName;
    private String lastName;
    private int grade;

    public Trainee(String firstName, String lastName, int grade) throws TraineeException {
        setFirstName(firstName);
        setLastName(lastName);
        setGrade(grade);
    }

    public void setFirstName(String firstName) throws TraineeException {
        if (firstName.isEmpty()) {
            throw new TraineeException(TraineeErrorCodes.TRAINEE_WRONG_FIRSTNAME);
        }
        this.firstName = firstName;
    }

    public void setLastName(String lastName) throws TraineeException {
        if (lastName.isEmpty()) {
            throw new TraineeException(TraineeErrorCodes.TRAINEE_WRONG_LASTNAME);
        }
        this.lastName = lastName;
    }

    public void setGrade(int grade) throws TraineeException {
        if (grade < 1 && grade > 5) {
            throw new TraineeException(TraineeErrorCodes.TRAINEE_WRONG_GRADE);
        }
        this.grade = grade;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getGrade() {
        return grade;
    }

    public String getFullName() {
        return getFirstName() + " " + getLastName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trainee trainee = (Trainee) o;
        return grade == trainee.grade &&
                Objects.equals(firstName, trainee.firstName) &&
                Objects.equals(lastName, trainee.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, grade);
    }

    @Override
    public int compareTo(Trainee trainee) {
        return getFirstName().compareTo(trainee.getFirstName());
    }

    @Override
    public String toString() {
        return getFullName() + ": " + getGrade();
    }
}
