package ru.omsu.imit.course3.trainee;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class TraineeList {
    private List<Trainee> trainees;

    public TraineeList() {
        trainees = new ArrayList<>();
    }

    public List<Trainee> getTrainees() {
        return trainees;
    }

    public void setTrainees(List<Trainee> trainees) {
        this.trainees = trainees;
    }

    public void addTrainee(Trainee trainee) {
        trainees.add(trainee);
    }

    public void reverseTraineeList() {
        Collections.reverse(trainees);
    }

    public void rotateTraineeList(int position) {
        Collections.rotate(trainees, position);
    }

    public void shuffleTraineeList() {
        Collections.shuffle(trainees);
    }

    public List<Trainee> getTraineeWithMaxGrade() throws TraineeException {
        int max = trainees.stream().mapToInt(Trainee::getGrade).max().orElse(-1);
        List<Trainee> traineeWithMaxGrade = trainees.stream().filter((t) -> t.getGrade() == max).collect(toList());
        if (traineeWithMaxGrade.isEmpty()) {
            throw new TraineeException(TraineeErrorCodes.TRAINEE_NOT_FOUND);
        }
        return traineeWithMaxGrade;
    }

    public void sortTraineeListByGrade() {
        trainees.sort(Comparator.comparingInt(Trainee::getGrade).reversed());
    }

    public void sortTraineeListByFirstName() {
        trainees.sort(Comparator.comparing(Trainee::getFirstName));
    }

    public Trainee getTraineeByFirstName(String firstName) throws TraineeException {
        for (Trainee trainee : trainees) {
            if (trainee.getFirstName().equals(firstName)) {
                return trainee;
            }
        }
        throw new TraineeException(TraineeErrorCodes.TRAINEE_NOT_FOUND);
    }
}
