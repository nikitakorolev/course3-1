package ru.omsu.imit.course3.fourth.resources;

import com.google.gson.Gson;
import ru.omsu.imit.course3.fourth.service.PersonService;
import ru.omsu.imit.course3.third.Person;
import ru.omsu.imit.course3.third.PersonRequest;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/api")
public class PersonResource {
    private static PersonService personService = new PersonService();

    @POST
    @Path("/person")
    @Consumes("application/json")
    @Produces("application/json")
    public Response addPerson(String json) {
        return personService.addPerson(json);
    }

/*
    @GET
    @Path("/person/{firstName}&{lastName}&{age}")
    @Produces("application/json")
    public Response getPerson(
            @PathParam("firstName") String firstName,
            @PathParam("lastName") String lastName,
            @PathParam("age") Integer age) {
        PersonRequest personRequest = new PersonRequest(firstName, lastName, age);
        return personService.getPerson(new Gson().toJson(personRequest, PersonRequest.class));
    }*/

    @GET
    @Path("/person")
    @Produces("application/json")
    public Response getPerson(
            @QueryParam("firstName") String firstName,
            @QueryParam("lastName") String lastName,
            @QueryParam("age") Integer age
            ) {
        if (firstName.equals("")) {
            firstName = null;
        }
        if (lastName.equals("")) {
            lastName = null;
        }
        PersonRequest personRequest = new PersonRequest(firstName, lastName, age);
        return personService.getPerson(new Gson().toJson(personRequest, PersonRequest.class));
    }
}
