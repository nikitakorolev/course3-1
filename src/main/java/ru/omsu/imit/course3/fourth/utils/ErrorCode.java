package ru.omsu.imit.course3.fourth.utils;

public enum ErrorCode {
    SUCCESS("", ""),
    PERSON_NOT_FOUND("person", "Person not found: %s"),
    NULL_REQUEST("json", "Null request"),
    JSON_PARSE_EXCEPTION("json", "Json parse exception: %s"),
    WRONG_URL("url", "Wrong URL"),
    METHOD_NOT_ALLOWED("url", "Method not allowed"),
    UNKNOWN_ERROR("error", "Unknown error");

    private String field;
    private String message;

    private ErrorCode(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public String getMessage() {
        return message;
    }
}
