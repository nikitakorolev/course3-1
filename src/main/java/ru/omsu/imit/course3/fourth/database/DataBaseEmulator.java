package ru.omsu.imit.course3.fourth.database;

import ru.omsu.imit.course3.fourth.model.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DataBaseEmulator {
    private static List<Person> list = new ArrayList<>();
    // private static AtomicInteger atomicInteger = new AtomicInteger(1);

    public static Person addPerson(Person person) {
        synchronized (list) {
            list.add(person);
        }
        return person;
    }

    public static Person getPerson(Person person) {
        synchronized (list) {
            for (Person p : list) {
                if (person.compare(p)) {
                    return p;
                }
            }
            return null;
        }
    }
}
