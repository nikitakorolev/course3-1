package ru.omsu.imit.course3.fourth.exception;

import ru.omsu.imit.course3.fourth.utils.ErrorCode;

public class PersonException extends Exception {
    private ErrorCode errorCode;
    private String param;

    public PersonException(ErrorCode errorCode, String param) {
        this.errorCode = errorCode;
        this.param = param;
    }

    public PersonException(ErrorCode errorCode) {
        this(errorCode, null);
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public String getParam() {
        if (param != null) {
            return String.format(errorCode.getMessage(), param);
        }
        else {
            return errorCode.getMessage();
        }
    }
}
