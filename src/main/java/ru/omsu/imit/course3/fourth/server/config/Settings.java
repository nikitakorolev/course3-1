package ru.omsu.imit.course3.fourth.server.config;

public class Settings {
    private static int restHttpPort = 8888;

    public static int getRestHttpPort() {
        return restHttpPort;
    }
}
