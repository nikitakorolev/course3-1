package ru.omsu.imit.course3.fourth.rest.request;

import ru.omsu.imit.course3.fourth.model.Person;

public class PersonRequest {
    private String firstName;
    private String lastName;
    private Integer age;

    public PersonRequest(String firstName, String lastName, Integer age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public PersonRequest(Person person) {
        this(person.getFirstName(), person.getLastName(), person.getAge());
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getAge() {
        return age;
    }


}
