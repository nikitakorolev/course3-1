package ru.omsu.imit.course3.fourth.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.course3.fourth.dao.PersonDAO;
import ru.omsu.imit.course3.fourth.daoimpl.PersonDAOImpl;
import ru.omsu.imit.course3.fourth.exception.PersonException;
import ru.omsu.imit.course3.fourth.model.Person;
import ru.omsu.imit.course3.fourth.rest.request.PersonRequest;
import ru.omsu.imit.course3.fourth.rest.response.PersonInfoResponse;
import ru.omsu.imit.course3.fourth.utils.PersonUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class PersonService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private static final Gson GSON = new GsonBuilder().create();
    private PersonDAO personDAO = new PersonDAOImpl();

    public Response addPerson(String json) {
        LOGGER.debug("Add person " + json);
        try {
            PersonRequest request = PersonUtils.getClassInstanceFronJson(GSON, json, PersonRequest.class);
            Person person = new Person(request);
            Person addedPerson = personDAO.addPerson(person);
            String response = GSON.toJson(new PersonInfoResponse(addedPerson.getFirstName(), addedPerson.getLastName(), addedPerson.getAge()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (PersonException ex) {
            return PersonUtils.failureResponse(ex);
        }
    }

    public Response getPerson(String json) {
        LOGGER.debug("Get person " + json);
        try {
            PersonRequest request = PersonUtils.getClassInstanceFronJson(GSON, json, PersonRequest.class);
            Person person = new Person(request);
            Person getedPerson = personDAO.getPerson(person);
            String response = GSON.toJson(new PersonInfoResponse(getedPerson.getFirstName(), getedPerson.getLastName(), getedPerson.getAge()));
            return Response.ok(response, MediaType.APPLICATION_JSON).build();
        } catch (PersonException ex) {
            return PersonUtils.failureResponse(ex);
        }
    }
}
