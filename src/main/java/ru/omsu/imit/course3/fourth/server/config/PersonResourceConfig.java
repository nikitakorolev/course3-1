package ru.omsu.imit.course3.fourth.server.config;

import org.glassfish.jersey.server.ResourceConfig;

public class PersonResourceConfig extends ResourceConfig {
    public PersonResourceConfig() {
        packages("ru.omsu.imit.course3.fourth.resources",
                "ru.omsu.imit.course3.fourth.rest.mappers");
    }
}
