package ru.omsu.imit.course3.fourth.dao;

import ru.omsu.imit.course3.fourth.exception.PersonException;
import ru.omsu.imit.course3.fourth.model.Person;

public interface PersonDAO {
    public Person addPerson(Person person);
    public Person getPerson(Person person) throws PersonException;
}
