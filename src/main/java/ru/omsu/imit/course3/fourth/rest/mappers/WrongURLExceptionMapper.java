package ru.omsu.imit.course3.fourth.rest.mappers;

import ru.omsu.imit.course3.fourth.exception.PersonException;
import ru.omsu.imit.course3.fourth.utils.ErrorCode;
import ru.omsu.imit.course3.fourth.utils.PersonUtils;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class WrongURLExceptionMapper implements ExceptionMapper<NotFoundException> {
    @Override
    public Response toResponse(NotFoundException e) {
        return PersonUtils.failureResponse(Status.NOT_FOUND, new PersonException(ErrorCode.WRONG_URL));
    }
}
