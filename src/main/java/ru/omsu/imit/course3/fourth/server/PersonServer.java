package ru.omsu.imit.course3.fourth.server;

import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.omsu.imit.course3.fourth.server.config.PersonResourceConfig;
import ru.omsu.imit.course3.fourth.server.config.Settings;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

public class PersonServer {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonServer.class);
    private static Server jettyServer;

    private static void attachShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                stopServer();
            }
        });
    }

    public static void createServer() {
        URI baseUri = UriBuilder.fromUri("http://localhost/").port(Settings.getRestHttpPort()).build();
        PersonResourceConfig config = new PersonResourceConfig();
        jettyServer = JettyHttpContainerFactory.createServer(baseUri, config);
        LOGGER.info("Server started at port " + Settings.getRestHttpPort());
    }

    public static void stopServer() {
        LOGGER.info("Stopping server");
        try {
            jettyServer.stop();
            jettyServer.destroy();
        } catch (Exception ex) {
            LOGGER.error("Error stopping server", ex);
            System.exit(1);
        }
        LOGGER.info("Server stopped");
    }

    public static void main(String[] args) {
        attachShutDownHook();
        createServer();
    }
}
