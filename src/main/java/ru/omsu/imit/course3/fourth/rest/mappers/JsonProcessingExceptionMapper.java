package ru.omsu.imit.course3.fourth.rest.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;
import ru.omsu.imit.course3.fourth.exception.PersonException;
import ru.omsu.imit.course3.fourth.utils.ErrorCode;
import ru.omsu.imit.course3.fourth.utils.PersonUtils;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class JsonProcessingExceptionMapper implements ExceptionMapper<JsonProcessingException> {
    @Override
    public Response toResponse(JsonProcessingException e) {
        return PersonUtils.failureResponse(Status.BAD_REQUEST, new PersonException(ErrorCode.JSON_PARSE_EXCEPTION));
    }
}
