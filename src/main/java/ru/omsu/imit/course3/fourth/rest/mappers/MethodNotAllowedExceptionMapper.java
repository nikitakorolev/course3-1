package ru.omsu.imit.course3.fourth.rest.mappers;

import ru.omsu.imit.course3.fourth.exception.PersonException;
import ru.omsu.imit.course3.fourth.utils.ErrorCode;
import ru.omsu.imit.course3.fourth.utils.PersonUtils;

import javax.ws.rs.NotAllowedException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class MethodNotAllowedExceptionMapper implements ExceptionMapper<NotAllowedException> {
    @Override
    public Response toResponse(NotAllowedException e) {
        return PersonUtils.failureResponse(Status.METHOD_NOT_ALLOWED, new PersonException(ErrorCode.METHOD_NOT_ALLOWED));
    }
}
