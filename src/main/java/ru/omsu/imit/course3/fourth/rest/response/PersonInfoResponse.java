package ru.omsu.imit.course3.fourth.rest.response;

import ru.omsu.imit.course3.fourth.model.Person;

import java.util.Objects;

public class PersonInfoResponse extends BaseResponseObject {
    private String firstName;
    private String lastName;
    private Integer age;

    public PersonInfoResponse(String firstName, String lastName, Integer age) {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public PersonInfoResponse(Person person) {
        this(person.getFirstName(), person.getLastName(), person.getAge());
    }

    protected PersonInfoResponse() {

    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getAge() {
        return age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonInfoResponse that = (PersonInfoResponse) o;
        return Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(age, that.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, age);
    }
}
