package ru.omsu.imit.course3.fourth.daoimpl;

import ru.omsu.imit.course3.fourth.dao.PersonDAO;
import ru.omsu.imit.course3.fourth.database.DataBaseEmulator;
import ru.omsu.imit.course3.fourth.exception.PersonException;
import ru.omsu.imit.course3.fourth.model.Person;
import ru.omsu.imit.course3.fourth.utils.ErrorCode;

public class PersonDAOImpl implements PersonDAO {
    @Override
    public Person addPerson(Person person) {
        return DataBaseEmulator.addPerson(person);
    }

    @Override
    public Person getPerson(Person person) throws PersonException {
        Person person1 = DataBaseEmulator.getPerson(person);
        if (person1 == null) {
            throw new PersonException(ErrorCode.PERSON_NOT_FOUND, person.toString());
        }
        return person1;
    }
}
