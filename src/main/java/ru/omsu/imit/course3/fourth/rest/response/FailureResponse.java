package ru.omsu.imit.course3.fourth.rest.response;

import ru.omsu.imit.course3.fourth.utils.ErrorCode;

public class FailureResponse extends BaseResponseObject {
    private ErrorCode errorCode;
    private String message;

    public FailureResponse(ErrorCode errorCode, String message) {
        super();
        this.errorCode = errorCode;
        this.message = message;
    }

    public FailureResponse(ErrorCode errorCode) {
        this(errorCode, "");
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }
}
