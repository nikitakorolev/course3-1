package ru.omsu.imit.course3.fourth.utils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.commons.lang3.StringUtils;
import ru.omsu.imit.course3.fourth.exception.PersonException;
import ru.omsu.imit.course3.fourth.rest.response.FailureResponse;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class PersonUtils {
    private static final Gson GSON = new Gson();

    public static <T> T getClassInstanceFronJson(Gson gson, String json, Class<T> clazz) throws PersonException {
        if (StringUtils.isEmpty(json)) {
            throw new PersonException(ErrorCode.NULL_REQUEST);
        }
        try {
            return gson.fromJson(json, clazz);
        } catch (JsonSyntaxException ex) {
            throw new PersonException(ErrorCode.JSON_PARSE_EXCEPTION, json);
        }
    }

    public static Response failureResponse(Status status, PersonException ex) {
        return Response.status(status).entity(GSON.toJson(new FailureResponse(ex.getErrorCode(), ex.getMessage()))).build();
    }

    public static Response failureResponse(PersonException ex) {
        return failureResponse(Status.BAD_REQUEST, ex);
    }
}
