package ru.omsu.imit.course3;

import java.util.ArrayList;

public class CityList {
    private ArrayList<City> list;

    public CityList() {
        this.list = new ArrayList<>();
    }

    public CityList(ArrayList<City> list) {
        this.list = list;
    }

    public void add(City c) {
        this.list.add(c);
    }

    public void removeOf(int index) {
        this.list.remove(index);
    }

    public void remove(City c) {
        City tmp = new City();
        for (int i = 0; i < this.list.size(); i++) {
            if (this.list.get(i).equals(c)) {
                list.remove(i);
            }
        }
    }
}