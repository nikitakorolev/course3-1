package ru.omsu.imit.course3.thread.concurrenthashmap;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ConcurrentHashMap<K, V> {
    private HashMap<K, V> map;
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    public ConcurrentHashMap() {
        map = new HashMap<>();
    }

    public ConcurrentHashMap(HashMap map) {
        this.map = map;
    }

    public boolean remove(K key, V value) {
        try {
            lock.writeLock().lock();
            return map.remove(key, value);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public boolean replace(K key, V oldValue, V newValue) {
        try {
            lock.writeLock().lock();
            return map.replace(key, oldValue, newValue);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public int size() {
        try {
            lock.readLock().lock();
            return map.size();
        } finally {
            lock.readLock().unlock();
        }
    }

    public boolean isEmpty() {
        try {
            lock.readLock().lock();
            return map.isEmpty();
        } finally {
            lock.readLock().unlock();
        }
    }

    public boolean containsKey(Object key) {
        try {
            lock.readLock().lock();
            return map.containsKey(key);
        } finally {
            lock.readLock().unlock();
        }
    }


    public boolean containsValue(Object value) {
        try {
            lock.readLock().lock();
            return map.containsValue(value);
        } finally {
            lock.readLock().unlock();
        }
    }

    public V get(Object key) {
        try {
            lock.readLock().lock();
            return map.get(key);
        } finally {
            lock.readLock().unlock();
        }
    }

    public V put(K key, V value) {
        try {
            lock.writeLock().lock();
            return map.put(key, value);
        } finally {
            lock.writeLock().unlock();
        }
    }


    public V remove(Object key) {
        try {
            lock.writeLock().lock();
            return map.remove(key);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public void clear() {
        map.clear();
    }

    public Collection values() {
        try {
            lock.readLock().lock();
            return map.values();
        } finally {
            lock.readLock().unlock();
        }
    }

    public Set<Map.Entry<K, V>> entrySet() {
        try {
            lock.readLock().lock();
            return map.entrySet();
        } finally {
            lock.readLock().unlock();
        }
    }
}