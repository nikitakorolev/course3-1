package ru.omsu.imit.course3.thread.task;

import org.omg.IOP.TAG_ALTERNATE_IIOP_ADDRESS;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class ExecutorThread extends Thread {
    private BlockingQueue<Task> taskQueue;
    private String name;

    public ExecutorThread(String name, BlockingQueue<Task> taskQueue) {
        this.name = name;
        this.taskQueue = taskQueue;
    }

    @Override
    public void run() {
        while(true) {
            try {
                Task task = taskQueue.take();
                if (task.getNumber() == -1) {
                    return;
                }
                System.out.println(name + " take task, size" + taskQueue.size());
                task.execute();
                Thread.sleep(new Random().nextInt(124) + 2000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
