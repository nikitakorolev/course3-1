package ru.omsu.imit.course3.thread.task;

import com.beust.jcommander.JCommander;

import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class TaskDemo {
    public static void main(String[] args) {
        Args arguments = new Args();
        JCommander jc = JCommander.newBuilder()
                .addObject(arguments)
                .build();
        jc.parse(args);

        final BlockingQueue<Task> queue = new LinkedBlockingDeque<>();

        if (arguments.developers == null || arguments.executors == null) {
            StringBuilder sb = new StringBuilder();
            jc.usage(sb);
            System.out.println(sb.toString());
            return;
        }

        int developerCount = arguments.developers;
        int executorCount = arguments.executors;

        DeveloperThread[] developers = new DeveloperThread[developerCount];

        for (int i = 0; i < developerCount; i++) {
            developers[i] = new DeveloperThread("developer " + i, queue);
        }

        ExecutorThread[] executors = new ExecutorThread[executorCount];

        for (int i = 0; i < executorCount; i++) {
            executors[i] = new ExecutorThread("executor " + i, queue);
        }

        Arrays.stream(developers).forEach(thread -> thread.start());

        try {
            for (DeveloperThread d : developers) {
                d.join();
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        if (queue.size() > 40) {
            ExecutorThread[] executors2 = new ExecutorThread[executorCount + 10];
            for (int i = 0; i < executorCount + 10; i++) {
                if (i < executors.length) {
                    executors2[i] = executors[i];
                } else {
                    executors2[i] = new ExecutorThread("executor " + i, queue);
                }
            }
            Arrays.stream(executors2).forEach(thread -> thread.start());
        } else {
            Arrays.stream(executors).forEach(thread -> thread.start());
        }

        try {
            if (queue.size() < 5) {
                for (int i = 0; i < executorCount; i++) {
                    queue.put(new Task(-1));
                }
                for (ExecutorThread e : executors) {
                    e.join();
                }
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        /*
        try {
            for (DeveloperThread d : developers) {
                d.join();
            }
            for (int i = 0; i < executorCount; i++) {
                queue.put(new Task(-1));
            }
            for (ExecutorThread e : executors) {
                e.join();
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }*/
    }
}
