package ru.omsu.imit.course3.thread.data;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class WritterThread extends Thread {
    private BlockingQueue<Data> dataQueue;
    private String name;

    public WritterThread(String name, BlockingQueue<Data> dataQueue) {
        this.name = name;
        this.dataQueue = dataQueue;
    }

    @Override
    public void run() {
        for (int i = 0; i < 4; i++) {
            try {
                Data data = new Data();
                dataQueue.put(data);
                System.out.println(name + " put to queue: " + data + " queue size: " + dataQueue.size());
                Thread.sleep(new Random().nextInt(140));
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
