package ru.omsu.imit.course3.thread.concurrenthashmap;

import java.util.Random;

public class PutThread implements Runnable {
    private ConcurrentHashMap map;

    public PutThread(ConcurrentHashMap map) {
        this.map = map;
        new Thread(this).start();
    }

    @Override
    public void run() {
        Random random = new Random();
        for (int i = 0; i < random.nextInt(20); i++) {
            map.put("Key" + i, i);
        }
    }
}
