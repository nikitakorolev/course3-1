package ru.omsu.imit.course3.thread.message2;

public class Message {
    private String emailAddress;
    private String sender;
    private String subject;
    private String body;

    public Message(String emailAddress, String sender, String subject, String body) {
        this.emailAddress = emailAddress;
        this.sender = sender;
        this.subject = subject;
        this.body = body;
    }

    @Override
    public String toString() {
        return  "\nemailAddress = " + emailAddress + '\n' +
                "sender = " + sender + '\n' +
                "subject = " + subject + '\n' +
                "body = " + body + '\n';
    }
}
