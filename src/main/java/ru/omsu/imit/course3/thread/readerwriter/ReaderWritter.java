package ru.omsu.imit.course3.thread.readerwriter;

import java.util.concurrent.locks.ReadWriteLock;

public class ReaderWritter {
    private String line;
    private ReadWriteLock lock;

    public ReaderWritter(ReadWriteLock readWriteLock) {
        this.line = "ReaderWritterInitialize";
        System.out.println(line);
        this.lock = readWriteLock;
    }

    public void write() {
        try {
            lock.writeLock().lock();
            line = "new book";
            System.out.println("The writer wrote something");
        } finally {
            lock.writeLock().unlock();
        }
    }

    public void read() {
        try {
            lock.readLock().lock();
            System.out.println("Reader read everything: " + line);
        } finally {
            lock.readLock().unlock();
        }
    }
}
