package ru.omsu.imit.course3.thread.pingpongcondition;

public class PingThread implements Runnable {
    private PingPongLock pingPong;

    public PingThread(PingPongLock pingPong) {
        this.pingPong = pingPong;
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (true) {
            pingPong.ping();
        }
    }
}
