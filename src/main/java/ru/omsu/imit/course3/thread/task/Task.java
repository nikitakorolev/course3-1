package ru.omsu.imit.course3.thread.task;

import ru.omsu.imit.course3.thread.Executable;

public class Task implements Executable {
    private int number;

    public Task(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public void execute() {
        System.out.println("task: " + number + " done");
    }
}
