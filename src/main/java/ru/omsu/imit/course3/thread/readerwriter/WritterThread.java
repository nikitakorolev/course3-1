package ru.omsu.imit.course3.thread.readerwriter;

public class WritterThread extends Thread {
    private ReaderWritter readerWritter;

    public WritterThread(ReaderWritter readerWritter) {
        this.readerWritter = readerWritter;
    }

    @Override
    public void run() {
        readerWritter.write();
    }
}
