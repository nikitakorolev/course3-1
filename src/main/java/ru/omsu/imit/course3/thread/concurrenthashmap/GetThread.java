package ru.omsu.imit.course3.thread.concurrenthashmap;

public class GetThread implements Runnable {
    private ConcurrentHashMap map;

    public GetThread(ConcurrentHashMap map) {
        this.map = map;
        new Thread(this).start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(map.get("Key" + i));
        }
    }
}
