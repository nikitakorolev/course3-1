package ru.omsu.imit.course3.thread.formatter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Formatter {
    private ThreadLocal<SimpleDateFormat> localSimpleDateFormat;

    public Formatter() {
        localSimpleDateFormat = new ThreadLocal<SimpleDateFormat>() {
            protected SimpleDateFormat initialValue() {
                return new SimpleDateFormat();
            }
        };
    }

    public Formatter(final String pattern) {
        localSimpleDateFormat = new ThreadLocal<SimpleDateFormat>() {
            protected SimpleDateFormat initialValue() {
                return new SimpleDateFormat(pattern);
            }
        };
    }

    public String format(Date date) {
        String stringDate = localSimpleDateFormat.get().format(date);
        System.out.println(stringDate);
        return stringDate;
    }
}
