package ru.omsu.imit.course3.thread;

public class ThreadDemo {
    public static void main(String[] args) {
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println("Main thread:" + i);
                Thread.sleep(500);
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        System.out.println("Main thread exiting");
    }
}
