package ru.omsu.imit.course3.thread.data;

import com.beust.jcommander.JCommander;

import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class DataQueueDemo {
    public static void main(String[] args) {
        Args arguments = new Args();
        JCommander jc = JCommander.newBuilder().addObject(arguments).build();
        jc.setProgramName("DataQueueDemo");
        jc.parse(args);

        final BlockingQueue<Data> queue = new LinkedBlockingDeque<>();

        if (arguments.writers == null || arguments.readers == null) {
            StringBuilder sb = new StringBuilder();
            jc.usage(sb);
            System.out.println(sb.toString());
            return;
        }

        int writerCount = arguments.writers;
        int readerCount = arguments.readers;

        WritterThread[] writters = new WritterThread[writerCount];
        for (int i = 0; i < writerCount; i++) {
            writters[i] = new WritterThread("Writter " + i, queue);
        }

        ReaderThread[] readers = new ReaderThread[readerCount];
        for (int i = 0; i < readerCount; i++) {
            readers[i] = new ReaderThread("Reader " + i, queue);
        }

        Arrays.stream(writters).forEach(thread -> thread.start());
        Arrays.stream(readers).forEach(thread -> thread.start());


    }
}
