package ru.omsu.imit.course3.thread.readerwriter;

public class ReaderThread extends Thread {
    private ReaderWritter readerWritter;

    public ReaderThread(ReaderWritter readerWritter) {
        this.readerWritter = readerWritter;
    }

    @Override
    public void run() {
        readerWritter.read();
    }
}
