package ru.omsu.imit.course3.thread;

public interface Executable {
    void execute();
}
