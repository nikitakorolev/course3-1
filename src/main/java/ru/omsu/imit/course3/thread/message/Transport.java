package ru.omsu.imit.course3.thread.message;

import ru.omsu.imit.course3.trainee.Trainee;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Transport {
    private File fileForEmails;

    public Transport(File fileForEmails) {
        this.fileForEmails = fileForEmails;
    }

    public void send(Message message) {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileForEmails, true), "UTF-8"))) {
            bw.write(message.getEmailAddress() + " "
                    + message.getSender() + " "
                    + message.getSubject() + " "
                    + message.getBody());
            bw.newLine();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void sendMessage(File fileWithEmail, File fileToWriteEmail) {
        ExecutorService es = Executors.newFixedThreadPool(10);

        Transport transport = new Transport(fileToWriteEmail);
        List<Future<?>> futures = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(
                                 new InputStreamReader(
                                 new FileInputStream(fileWithEmail), "UTF-8"))) {
            String email;
            while ((email = br.readLine()) != null) {
                futures.add(es.submit(new SendThread(transport, new Message(email, "sender", "subject", "bodybodybody body"))));
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        while (!futures.stream().allMatch(future -> future.isDone())) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
