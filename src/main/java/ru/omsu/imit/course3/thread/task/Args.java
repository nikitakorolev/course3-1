package ru.omsu.imit.course3.thread.task;

import com.beust.jcommander.Parameter;

import java.util.ArrayList;
import java.util.List;

public class Args {
    @Parameter
    private List<String> parameters = new ArrayList<>();

    @Parameter(names = {"-d"}, description = "developers count")
    public Integer developers;

    @Parameter(names = {"-e"}, description = "executors count")
    public Integer executors;
}
