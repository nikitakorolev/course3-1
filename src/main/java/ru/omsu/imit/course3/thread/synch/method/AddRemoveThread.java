package ru.omsu.imit.course3.thread.synch.method;

public class AddRemoveThread implements Runnable {
    private ListSynch listSynch;
    private Thread t;
    private Action action;

    public AddRemoveThread(ListSynch listSynch, Action action) {
        this.listSynch = listSynch;
        this.action = action;
        this.t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            if (action.equals(Action.ADD)) {
                listSynch.add((int) (Math.random() * 250));
            }
            if (action.equals(Action.REMOVE)) {
                if (listSynch.getSize() > 0) {
                    listSynch.remove((int) (Math.random() * (listSynch.getSize() - 1)));
                } else {
                    i--;
                }
            }
        }
    }
}
