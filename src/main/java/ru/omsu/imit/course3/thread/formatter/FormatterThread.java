package ru.omsu.imit.course3.thread.formatter;

import java.util.Date;

public class FormatterThread implements Runnable {
    private Formatter formatter;
    private Date date;

    public FormatterThread(Formatter formatter, Date date) {
        this.formatter = formatter;
        this.date = date;
        new Thread(this).start();
    }

    @Override
    public void run() {
        formatter.format(date);
    }
}
