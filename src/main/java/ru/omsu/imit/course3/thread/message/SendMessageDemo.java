package ru.omsu.imit.course3.thread.message;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SendMessageDemo {
    public static void main(String[] args) {
        try {
            File file = new File("C:\\Users\\Nekit\\Desktop\\servers\\test.txt");
            file.createNewFile();

            List<String> emails = new ArrayList<>();
            for (int i = 0; i < 20000; i++) {
                emails.add("email" + i + "gmail.com");
            }

            EmailFile.writeEmailToTextFile(file, emails);

            File file1 = new File("C:\\Users\\Nekit\\Desktop\\servers\\test2.txt");
            file1.createNewFile();

            Transport.sendMessage(file, file1);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
