package ru.omsu.imit.course3.thread.pingpong;

import ru.omsu.imit.course3.thread.pingpong.PingPong;
import ru.omsu.imit.course3.thread.pingpong.PingThread;
import ru.omsu.imit.course3.thread.pingpong.PongThread;

public class PingPongDemo {
    public static void main(String[] args) {
        PingPong pingPong = new PingPong();
        new PongThread(pingPong);
        new PingThread(pingPong);
    }
}
