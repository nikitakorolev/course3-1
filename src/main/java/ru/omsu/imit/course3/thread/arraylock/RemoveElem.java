package ru.omsu.imit.course3.thread.arraylock;

public class RemoveElem implements Runnable {
    private SafeArrayList safeArrayList;

    public RemoveElem(SafeArrayList safeArrayList) {
        this.safeArrayList = safeArrayList;
        new Thread(this).start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            if (safeArrayList.getSize() > 0) {
                safeArrayList.remove((int) (Math.random() * (safeArrayList.getSize() - 1)));
            } else {
                i--;
            }
        }
    }
}
