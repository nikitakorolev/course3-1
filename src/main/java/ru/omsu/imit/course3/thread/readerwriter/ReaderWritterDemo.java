package ru.omsu.imit.course3.thread.readerwriter;

import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReaderWritterDemo {
    public static void main(String[] args) {
        ReaderWritter readerWritter = new ReaderWritter(new ReentrantReadWriteLock());

        WritterThread writter = new WritterThread(readerWritter);
        writter.start();

        for (int i = 0; i < 10; i++) {
            new WritterThread(readerWritter).start();
        }

        for (int i = 0; i < 5; i++) {
            new ReaderThread(readerWritter).start();
        }
    }
}
