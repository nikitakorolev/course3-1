package ru.omsu.imit.course3.thread.synch.block;

import java.util.ArrayList;

public class ExternalSynchDemo {
    public static void main(String[] args) {
        ArrayList<Integer> array = new ArrayList<>();
        ArrayAdd arrayAdd = new ArrayAdd(array);
        ArrayRemove arrayRemove = new ArrayRemove(array);
    }
}
