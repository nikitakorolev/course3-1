package ru.omsu.imit.course3.thread.simplethread;

public class SimpleThread2 implements Runnable {
    private String name;
    public Thread t;

    public SimpleThread2(String name) {
        this.name = name;
        t = new Thread(this, name);
        System.out.println("New thread: " + t);
        t.start();
    }

    @Override
    public void run() {
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println(name + ": " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        System.out.println(name + " exiting");
    }
}
