package ru.omsu.imit.course3.thread.pingpongcondition;

public class PongThread implements Runnable {
    private PingPongLock pingPong;

    public PongThread(PingPongLock pingPong) {
        this.pingPong = pingPong;
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (true) {
            pingPong.pong();
        }
    }
}
