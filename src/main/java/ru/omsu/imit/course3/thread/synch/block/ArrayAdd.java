package ru.omsu.imit.course3.thread.synch.block;

import java.util.ArrayList;

public class ArrayAdd implements Runnable {
    private final ArrayList<Integer> array;
    private Thread t;

    public ArrayAdd(ArrayList<Integer> array) {
        this.array = array;
        this.t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            synchronized (array) {
                System.out.println("Добавили");
                array.add((int) Math.random() );
            }
        }
    }

    public Thread getT() {
        return t;
    }
}
