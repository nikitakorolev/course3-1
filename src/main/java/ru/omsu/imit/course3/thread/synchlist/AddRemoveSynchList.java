package ru.omsu.imit.course3.thread.synchlist;

import org.omg.CORBA.MARSHAL;
import ru.omsu.imit.course3.thread.synch.method.Action;

import java.util.Collections;
import java.util.List;

public class AddRemoveSynchList implements Runnable {
    private List<Integer> list;
    private Action action;
    private Thread t;

    public AddRemoveSynchList(List<Integer> list, Action action) {
        this.list = Collections.synchronizedList(list);
        this.action = action;
        this.t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            if (action.equals(Action.ADD)) {
                System.out.println("Добавдяем");
                list.add((int) (Math.random() * 250));
            }
            if (action.equals(Action.REMOVE)) {
                if (list.size() > 0) {
                    System.out.println("Удаляем");
                    list.remove((int) (Math.random() * (list.size() - 1)));
                } else {
                    i--;
                }
            }
        }
    }
}
