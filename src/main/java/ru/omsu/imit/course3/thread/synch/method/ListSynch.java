package ru.omsu.imit.course3.thread.synch.method;

import java.util.List;

public class ListSynch {
    private List<Integer> array;

    public ListSynch(List<Integer> array) {
        this.array = array;
    }

    public int getSize() {
        return array.size();
    }

    public synchronized void remove(int index) {
        System.out.println("Remove at: " + index + " elem: " + array.get(index));
        array.remove(index);
    }

    public synchronized void add(int item) {
        System.out.println("add at: " + array.size() + " item :" + item);
        array.add(item);
    }
}
