package ru.omsu.imit.course3.thread.message2;

import java.io.*;

public class Transport {
    private File fileForSent;

    public Transport(File fileForSent) {
        this.fileForSent = fileForSent;
    }

    public void send(Message message) {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileForSent, true), "UTF-8"))) {
            System.out.println(message.toString());
            bw.write(message.toString());
            bw.newLine();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
