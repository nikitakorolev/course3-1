package ru.omsu.imit.course3.thread.pingpongcondition;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PingPongLock {
    private Lock lock = new ReentrantLock();
    private Condition pinged = lock.newCondition();
    private Condition ponged = lock.newCondition();

    public void ping() {
        try {
            lock.lock();
            System.out.print("Ping");
            pinged.signalAll();
            ponged.await();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void pong() {
        try {
            lock.lock();
            System.out.println("Pong");
            ponged.signalAll();
            pinged.await();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
