package ru.omsu.imit.course3.thread.pingpongcondition;

import ru.omsu.imit.course3.thread.pingpongcondition.PingPongLock;
import ru.omsu.imit.course3.thread.pingpongcondition.PingThread;
import ru.omsu.imit.course3.thread.pingpongcondition.PongThread;

public class PingPongLockDemo {
    public static void main(String[] args) {
        PingPongLock pingPong = new PingPongLock();
        new PingThread(pingPong);
        new PongThread(pingPong);
    }
}
