package ru.omsu.imit.course3.thread.arraylock;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SafeArrayList {
    private Lock lock = new ReentrantLock();
    private List<Integer> list;

    public SafeArrayList(Lock lock, List<Integer> list) {
        this.lock = lock;
        this.list = list;
    }

    public Lock getLock() {
        return lock;
    }

    public int getSize() {
        return list.size();
    }

    public void add(int item) {
        lock.lock();
        try {
            System.out.println("add at: " + list.size() + " elem: " + item);
            list.add(item);
        } finally {
            lock.unlock();
        }

    }

    public void remove(int index) {
        lock.lock();
        try {
            System.out.println("remove from: " + index + " item: " + list.get(index));
            list.remove(index);
        } finally {
            lock.unlock();
        }
    }
}
