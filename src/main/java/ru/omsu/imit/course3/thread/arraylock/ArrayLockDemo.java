package ru.omsu.imit.course3.thread.arraylock;

import ru.omsu.imit.course3.thread.arraylock.AddElem;
import ru.omsu.imit.course3.thread.arraylock.RemoveElem;
import ru.omsu.imit.course3.thread.arraylock.SafeArrayList;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class ArrayLockDemo {
    public static void main(String[] args) {
        ReentrantLock lock = new ReentrantLock();
        List<Integer> list = new ArrayList<>();
        SafeArrayList safeArrayList = new SafeArrayList(lock, list);
        new AddElem(safeArrayList);
        new RemoveElem(safeArrayList);
    }
}
