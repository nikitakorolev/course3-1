package ru.omsu.imit.course3.thread.synchlist;

import ru.omsu.imit.course3.thread.synch.method.Action;

import java.util.ArrayList;
import java.util.List;

public class SynchListDemo {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        AddRemoveSynchList add = new AddRemoveSynchList(list, Action.ADD);
        AddRemoveSynchList remove = new AddRemoveSynchList(list, Action.REMOVE);
    }
}
