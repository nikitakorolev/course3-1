package ru.omsu.imit.course3.thread.synch.block;

import java.util.ArrayList;

public class ArrayRemove implements Runnable {
    private final ArrayList<Integer> array;
    private Thread t;

    public ArrayRemove(ArrayList<Integer> array) {
        this.array = array;
        this.t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            synchronized (array) {
                if (array.size() > 0) {
                    System.out.println("Удалили");
                    array.remove((int) (Math.random() * (array.size() - 1)));
                } else {
                    i--;
                }
            }
        }
    }

    public Thread getT() {
        return t;
    }
}
