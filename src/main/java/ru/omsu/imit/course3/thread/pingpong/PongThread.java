package ru.omsu.imit.course3.thread.pingpong;

public class PongThread implements Runnable{
    private PingPong pong;

    public PongThread(PingPong pong) {
        this.pong = pong;
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (true) {
            pong.pong();
        }
    }
}
