package ru.omsu.imit.course3.thread.synch.method;

import java.util.ArrayList;

public class SynchMethodDemo {
    public static void main(String[] args) {
        ListSynch listSynch = new ListSynch(new ArrayList<Integer>());

        AddRemoveThread add = new AddRemoveThread(listSynch, Action.ADD);
        AddRemoveThread remove = new AddRemoveThread(listSynch, Action.REMOVE);
    }
}
