package ru.omsu.imit.course3.thread.formatter;

import ru.omsu.imit.course3.thread.formatter.Formatter;
import ru.omsu.imit.course3.thread.formatter.FormatterThread;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatterDemo {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        Formatter formatter = new Formatter("E yyyy.MM.dd 'at' hh:mm:ss a zzz");

        new FormatterThread(formatter, new Date());
        new FormatterThread(formatter, simpleDateFormat.parse("10-12-1997 14:00:00"));
        new FormatterThread(formatter, simpleDateFormat.parse("01-01-1999 2:00:00"));
        new FormatterThread(formatter, simpleDateFormat.parse("30-10-1977 18:00:00"));
        new FormatterThread(formatter, simpleDateFormat.parse("02-04-1970 13:00:00"));
    }
}
