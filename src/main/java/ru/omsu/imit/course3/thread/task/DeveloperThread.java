package ru.omsu.imit.course3.thread.task;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class DeveloperThread extends Thread {
    private BlockingQueue<Task> taskQueue;
    private String name;

    public DeveloperThread(String name, BlockingQueue<Task> taskQueue) {
        this.name = name;
        this.taskQueue = taskQueue;
    }

    @Override
    public void run() {
        for (int i = 0; i < 500; i++) {
            try {
                Task task = new Task(i);
                taskQueue.put(task);
                System.out.println(name + " put task " + i + " size queue " + taskQueue.size());
                // Thread.sleep(new Random().nextInt(100) + 25);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
