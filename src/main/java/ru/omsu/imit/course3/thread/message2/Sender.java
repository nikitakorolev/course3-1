package ru.omsu.imit.course3.thread.message2;

import java.io.File;

public class Sender implements Runnable {
    private Message message;
    private Transport transport;
    private File file;

    public Sender(Message message, File file) {
        this.message = message;
        this.file = file;
        transport = new Transport(file);
        new Thread(this).start();
    }

    @Override
    public void run() {
        transport.send(message);
    }
}
