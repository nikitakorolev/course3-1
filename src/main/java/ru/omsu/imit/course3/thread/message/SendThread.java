package ru.omsu.imit.course3.thread.message;

public class SendThread implements Runnable {
    private Transport transport;
    private Message message;

    public SendThread(Transport transport, Message message) {
        this.transport = transport;
        this.message = message;
    }

    @Override
    public void run() {
        transport.send(message);
    }
}
