package ru.omsu.imit.course3.thread.message2;

import java.io.*;

public class MessageDemo {
    public static void main(String[] args) {
        String email;
        String sender = "IamSender@gmail.com";
        String subject = "qwerty";
        String body = "body - body -- - -";

        File file = new File("C:\\Users\\Nekit\\Desktop\\servers\\test.txt");

        File forWrite = new File("C:\\Users\\Nekit\\Desktop\\servers\\test2.txt");

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
            Message message;

            while((email = br.readLine()) != null) {
                message = new Message(email, sender, subject, body);
                new Sender(message, forWrite);
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
