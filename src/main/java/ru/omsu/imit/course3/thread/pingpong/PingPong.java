package ru.omsu.imit.course3.thread.pingpong;

public class PingPong {
    private boolean valueSet = false;

    synchronized void ping() {
        while (!valueSet) {
            try {
                wait();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }

        System.out.println("ping");
        valueSet = false;
        notify();
    }

    synchronized void pong() {
        while (valueSet) {
            try {
                wait();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }

        System.out.println("pong");
        valueSet = true;
        notify();
    }
}
