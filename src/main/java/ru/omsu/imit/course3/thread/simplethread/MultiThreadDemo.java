package ru.omsu.imit.course3.thread.simplethread;

public class MultiThreadDemo {
    public static void main(String[] args) {
        SimpleThread1 simpleThread1 = new SimpleThread1("One");
        SimpleThread2 simpleThread2 = new SimpleThread2("Two");
        SimpleThread3 simpleThread3 = new SimpleThread3("Three");

        System.out.println("Thread one is alive: " + simpleThread1.t.isAlive());
        System.out.println("Thread two is alive: " + simpleThread2.t.isAlive());
        System.out.println("Thread three is alive: " + simpleThread3.t.isAlive());
        try {
            System.out.println("Waiting for threads to finish.");
            simpleThread1.t.join();
            simpleThread2.t.join();
            simpleThread3.t.join();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        System.out.println("Thread one is alive: " + simpleThread1.t.isAlive());
        System.out.println("Thread two is alive: " + simpleThread2.t.isAlive());
        System.out.println("Thread three is alive: " + simpleThread3.t.isAlive());

        System.out.println("Main thread exiting.");
    }
}
