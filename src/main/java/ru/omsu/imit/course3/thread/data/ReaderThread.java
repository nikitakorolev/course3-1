package ru.omsu.imit.course3.thread.data;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class ReaderThread extends Thread {
    private BlockingQueue<Data> dataQueue;
    private String name;

    public ReaderThread(String name, BlockingQueue<Data> dataQueue) {
        this.name = name;
        this.dataQueue = dataQueue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Data data = dataQueue.take();
                if (data.get().length == 0) {
                    return;
                }
                System.out.println(name + " take Data: " + Arrays.toString(data.get()));
                Thread.sleep(new Random().nextInt(200));
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
