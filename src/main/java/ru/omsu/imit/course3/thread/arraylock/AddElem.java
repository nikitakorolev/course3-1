package ru.omsu.imit.course3.thread.arraylock;

public class AddElem implements Runnable {
    private SafeArrayList safeArrayList;

    public AddElem(SafeArrayList safeArrayList) {
        this.safeArrayList = safeArrayList;
        new Thread(this).start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            safeArrayList.add((int) (Math.random() * 250));
        }
    }
}
