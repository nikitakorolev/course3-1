package ru.omsu.imit.course3.thread.message;

import java.io.*;
import java.util.List;

public class EmailFile {
    public static void writeEmailToTextFile(File file, List<String> email) {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"))) {
            for (String s : email) {
                bw.write(s);
                bw.newLine();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
