package ru.omsu.imit.course3.thread.concurrenthashmap;

public class ConcurrentThreadDemo {
    public static void main(String[] args) {
        ConcurrentHashMap<String, Integer> map = new ConcurrentHashMap<>();

        for (int i = 0; i < 5; i++) {
            new PutThread(map);
        }

        for (int i = 0; i < 10; i++) {
            new GetThread(map);
        }
    }
}
