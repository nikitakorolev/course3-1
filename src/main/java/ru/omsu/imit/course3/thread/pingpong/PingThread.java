package ru.omsu.imit.course3.thread.pingpong;

public class PingThread implements Runnable {
    private PingPong ping;

    public PingThread(PingPong ping) {
        this.ping = ping;
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (true) {
            ping.ping();
        }
    }
}
