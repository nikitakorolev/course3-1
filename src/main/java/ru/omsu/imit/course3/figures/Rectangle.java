package ru.omsu.imit.course3.figures;

import java.util.Objects;

public class Rectangle {
    private Point2D leftTop;
    private Point2D rigthBottom;

    public Rectangle(Point2D leftTop, Point2D rigthBottom) {
        this.leftTop = leftTop;
        this.rigthBottom = rigthBottom;
    }

    public Rectangle(int xLeft, int yTop, int xRight, int yBottom) {
        this(new Point2D(xLeft, yTop), new Point2D(xRight, yBottom));
    }

    public Point2D getLeftTop() {
        return leftTop;
    }

    public void setLeftTop(Point2D leftTop) {
        this.leftTop = leftTop;
    }

    public Point2D getRigthBottom() {
        return rigthBottom;
    }

    public void setRigthBottom(Point2D rigthBottom) {
        this.rigthBottom = rigthBottom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rectangle rectangle = (Rectangle) o;
        return Objects.equals(leftTop, rectangle.leftTop) &&
                Objects.equals(rigthBottom, rectangle.rigthBottom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(leftTop, rigthBottom);
    }
}
