package ru.omsu.imit.course3.file;

import com.google.gson.Gson;
import ru.omsu.imit.course3.figures.Rectangle;
import ru.omsu.imit.course3.trainee.Trainee;
import ru.omsu.imit.course3.trainee.TraineeException;

import java.io.*;

public class FileService {
    public static void writeRectangleToBinaryFile(File file, Rectangle rectangle) throws IOException {
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(file, true))) {
            dos.writeInt(rectangle.getLeftTop().getX());
            dos.writeInt(rectangle.getLeftTop().getY());
            dos.writeInt(rectangle.getRigthBottom().getX());
            dos.writeInt(rectangle.getRigthBottom().getY());
        }
    }

    public static Rectangle readRectangleFromBinaryFile(File file) throws IOException {
        try (DataInputStream dis = new DataInputStream(new FileInputStream(file))) {
            return new Rectangle(dis.readInt(), dis.readInt(),
                    dis.readInt(), dis.readInt());
        }
    }

    public static void writeRectangleArrayToBinaryFile(File file, Rectangle[] rectangles) throws IOException {
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(file, true))) {
            for (Rectangle rect : rectangles) {
                dos.writeInt(rect.getLeftTop().getX());
                dos.writeInt(rect.getLeftTop().getY());
                dos.writeInt(rect.getRigthBottom().getX());
                dos.writeInt(rect.getRigthBottom().getY());
            }
        }
    }

    public static Rectangle[] readRectangleArrayFromBinaryFileReverse(File file) throws IOException {
        try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
            Rectangle[] rectangles = new Rectangle[(int) file.length() / 16];
            for (int index = 0, rectNum = (int) (file.length() / 16); index < rectangles.length; index++, rectNum--) {
                raf.seek(rectNum * 16 - 16);
                rectangles[index] = new Rectangle(raf.readInt(), raf.readInt(),
                        raf.readInt(), raf.readInt());
            }
            return rectangles;
        }
    }

    public static void writeTraineeToTextFileThreeLines(File file, Trainee trainee) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"))) {
            bw.write(trainee.getFirstName());
            bw.newLine();
            bw.write(trainee.getLastName());
            bw.newLine();
            bw.write(String.valueOf(trainee.getGrade()));
        }
    }

    public static Trainee readTraineeFromTextFileThreeLine(File file) throws IOException, TraineeException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
            return new Trainee(br.readLine(), br.readLine(), Integer.valueOf(br.readLine()));
        } catch (NumberFormatException ex) {
            throw new NumberFormatException();
        }
    }

    public static void writeTraineeToTextFileOneLine(File file, Trainee trainee) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"))) {
            bw.write(trainee.getFullName() + " " + trainee.getGrade());
        }
    }

    public static Trainee readTraineeFromTextFileOneLine(File file) throws IOException, TraineeException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"))) {
            String[] data = br.readLine().split(" ");
            return new Trainee(data[0], data[1], Integer.valueOf(data[2]));
        } catch (NumberFormatException ex) {
            throw new NumberFormatException();
        }
    }

    public static void serializeTraineeToBinaryFile(File file, Trainee trainee) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(trainee);
        }
    }

    public static Trainee deserializeTraineeFromBinaryFile(File file) throws IOException, ClassNotFoundException {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            return (Trainee) ois.readObject();
        }
    }

    public static byte[] serializeTraieeToByteArrayOutputSream(Trainee trainee) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(trainee);
        byte[] buffer = baos.toByteArray();
        oos.close();
        baos.close();
        return buffer;
    }

    public static Trainee deserializeTraineeFromByteArrayInputStream(byte[] buffer) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
        ObjectInputStream ois = new ObjectInputStream(bais);
        Trainee trainee = (Trainee) ois.readObject();
        ois.close();
        bais.close();
        return trainee;
    }

    public static String serializeTraineeToJsonString(Trainee trainee) {
        return new Gson().toJson(trainee);
    }

    public static Trainee deserializeTraineeFromJsonString(String json) {
        return new Gson().fromJson(json, Trainee.class);
    }

    public static void serializeTraineeToJsonFile(File file, Trainee trainee) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write(serializeTraineeToJsonString(trainee));
        }
    }

    public static Trainee deserializeTraineeFromJsonFile(File file) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            return deserializeTraineeFromJsonString(br.readLine());
        }
    }

    public static byte[] readByteArrayFromBinaryFile(String fileName) throws IOException {
        File file = new File(fileName);
        byte[] result = new byte[(int) file.length()];
        try (FileInputStream fis = new FileInputStream(file)) {
            fis.read(result);
        }
        return result;
    }

    public static byte[] readByteArrayFromBinaryFile(File file) throws IOException {
        return readByteArrayFromBinaryFile(file.getPath());
    }

    public static void renameFileExtension(File file) throws IOException {
        File filesList[] = file.listFiles();
        for (int i = 0; i < filesList.length; i++) {
            String buf = filesList[i].getCanonicalPath();
            if (buf.endsWith(".dat")) {
                buf = buf.replaceFirst(".dat", ".bin");
                filesList[i].renameTo(new File(buf));
            }
        }
    }
}
