package ru.omsu.imit.course3.stream;

import ru.omsu.imit.course3.trainee.Trainee;

import java.io.*;

public class StreamService {
    public static void serializeTraineeToByteArrayOutputStream (ByteArrayOutputStream baos, Trainee trainee) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(trainee);
        }
    }

    public static Trainee deserializeTraineeFromByteArrayInputStream(ByteArrayInputStream bais) throws IOException, ClassNotFoundException {
        try(ObjectInputStream ois = new ObjectInputStream(bais)) {
            return (Trainee) ois.readObject();
        }
    }
}
