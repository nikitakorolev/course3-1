package ru.omsu.imit.course3.first;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.sun.deploy.perf.PerfRollup;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Server {
    private List<Person> personList = new ArrayList<>();

    public Server() {
        for (int i = 0; i < 5; i++) {
            personList.add(new Person("Имя " + i, "Фамилия " + i, i));
        }
    }

    public String get(String request) {
        try {
            for (Person p : personList) {
                String personJson = new Gson().toJson(p, Person.class);
                JsonObject personJsonObject = new JsonParser().parse(personJson).getAsJsonObject();
                JsonObject requestPerson = new JsonParser().parse(request).getAsJsonObject();
                Set<String> keySet = requestPerson.keySet();
                boolean flag = true;
                for (String s : keySet) {
                    if (!personJsonObject.get(s).equals(requestPerson.get(s))) {
                        flag = false;
                    }
                }
                if (flag) {
                    return personJson;
                }
            }
        } catch (JsonSyntaxException ex) {
            ex.printStackTrace();
        }
        return "{}";
    }

    public void add(String request) {
        try {
            personList.add(new Gson().fromJson(request, Person.class));
        } catch (JsonSyntaxException ex) {
            ex.printStackTrace();
        }
    }

    public int size() {
        return personList.size();
    }
}
