package ru.omsu.imit.course3.first;

import com.google.gson.Gson;

public class Client {
    private Server server = new Server();


    public void add(Person person) {
        server.add(new Gson().toJson(person, Person.class));
    }

    public Person get(String request) {
        return new Gson().fromJson(server.get(request), Person.class);
    }

    public int size() {
        return server.size();
    }
}
